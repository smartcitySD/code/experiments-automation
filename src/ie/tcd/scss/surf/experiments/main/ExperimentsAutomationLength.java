package ie.tcd.scss.surf.experiments.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExperimentsAutomationLength {

	public static void main(String[] args) {
		String approach = args[0];
		int mode = Integer.parseInt(args[1]);
		String base=args[2];
		int requests = Integer.parseInt(args[3]);
		int length = Integer.parseInt(args[4]);
		int hops = Integer.parseInt(args[5]);
		int create = Integer.parseInt(args[6]);
		String time = args[7];
		String summaryName = args[8];
		int adaptive = Integer.parseInt(args[9]);
		int rounds = Integer.parseInt(args[10]);
		int printErrors = Integer.parseInt(args[11]);
		int intervalUtilityComputation = Integer.parseInt(args[12]);
		int utilityTime = Integer.parseInt(args[13]);
		double learningRate = 0.0;
		int hiddenNodes = 0;
		int train = 0;
		String network = "none";
		int updateStart = 0;
		if(adaptive==4){
			learningRate = Double.parseDouble(args[14]);
			hiddenNodes = Integer.parseInt(args[15]);
			train = Integer.parseInt(args[16]);
			network = "none";
			if(train==0)
				network = args[17];
			updateStart = Integer.parseInt(args[18]);
		}
		/*String approach = "domain";
		int mode = 2;
		String base="/home/ubuntu/";
		int requests = 100;
		int length = 1;
		int hops = 5;
		int create = 1;
		String summaryName = "summary-urban-remove.xlsx";
		String time = "18000m";
		int adaptive = 4;
		int rounds = 10;
		int printErrors = 1;
		int intervalUtilityComputation = 1;
		int utilityTime = 10800;
		double learningRate = 0.01;
		int hiddenNodes = 12;
		int train = 0;
		String network = "none";
		int updateStart = 0;*/
		//String base="D://Christian/";
		String configFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_default.xml";
		String parametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_eval_parameters.xml";
		String pathToData = base + "Repositories/SmartCitySD/Data/";
		String pathToResults = base + "Repositories/SmartCitySD/Results/service-models-results-final/parameters/";
		//String pathToResults = base + "Repositories/SmartCitySD/Results/service-models-results/final/";
		String templateFile =  "";
		String actionsFile =  "";
		String simrunner = "";
		
		switch(approach) {
			case "location":
				if (adaptive==0)
					createSummaryFile("location",pathToResults, summaryName);
				templateFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/template-"+adaptive+".dat";
				actionsFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/actions.dat";
				simrunner =  base + "Repositories/Simonstrator/simonstrator-simrunner/";
				String experimentalSetupFile =  base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/";
				String initialParametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/initial-parameters/initial_parameters_"+mode+"_"+length+"_"+hops+"_"+adaptive+".dat";
				if(create==1)
					createInitialParametersFile("location",initialParametersFile);
				executeLocationBasedExperiments(approach,requests,configFile,parametersFile,experimentalSetupFile,initialParametersFile,pathToData,pathToResults,templateFile,actionsFile,simrunner,mode,length,hops,time,summaryName,adaptive,rounds,printErrors,intervalUtilityComputation,utilityTime);
			break;
			case "domain":
				if (adaptive==0)
					createSummaryFile("domain",pathToResults, summaryName);
				templateFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/template-"+adaptive+".dat";
				actionsFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/actions.dat";
				simrunner =  base + "Repositories/Simonstrator/simonstrator-simrunner/";
				experimentalSetupFile =  base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/";
				initialParametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/initial-parameters/initial_parameters_"+mode+"_"+length+"_"+hops+"_"+adaptive+".dat";
				if(create==1)
					createInitialParametersFile("domain",initialParametersFile);
				executeDomainBasedExperiments(approach,requests,configFile,parametersFile,experimentalSetupFile,initialParametersFile,pathToData,pathToResults,templateFile,actionsFile,simrunner,mode,length,hops,time,summaryName,adaptive,rounds,printErrors,intervalUtilityComputation,utilityTime);
			break;
			case "urban":
				if (adaptive==0)
					createSummaryFile("urban",pathToResults, summaryName);
				templateFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/template-"+adaptive+".dat";
				actionsFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/actions.dat";
				simrunner =  base + "Repositories/Simonstrator/simonstrator-simrunner/";
				experimentalSetupFile =  base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/";
				initialParametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/initial-parameters/initial_parameters_"+mode+"_"+length+"_"+hops+"_"+adaptive+".dat";
				if(create==1)
					createInitialParametersFile("urban",initialParametersFile);
				executeUrbanBasedExperiments(approach,requests,configFile,parametersFile,experimentalSetupFile,initialParametersFile,pathToData,pathToResults,templateFile,actionsFile,simrunner,mode,length,hops,time,summaryName,adaptive,rounds,printErrors,intervalUtilityComputation,utilityTime);
			break;
		case "adaptive":
				if (adaptive==0)
					createSummaryFile("adaptive",pathToResults, summaryName);
				templateFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/template-"+adaptive+".dat";
				actionsFile =   base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/actions.dat";
				simrunner =  base + "Repositories/Simonstrator/simonstrator-simrunner/";
				experimentalSetupFile =  base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/";
				initialParametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/initial-parameters/initial_parameters_"+mode+"_"+length+"_"+hops+"_"+adaptive+"_"+train+"_"+learningRate+"_"+hiddenNodes+"_"+network+".dat";
				if(create==1)
					createInitialParametersFile("adaptive",initialParametersFile);
				executeUrbanBasedAdaptiveExperiments(approach,requests,configFile,parametersFile,experimentalSetupFile,initialParametersFile,pathToData,pathToResults,templateFile,actionsFile,simrunner,mode,length,hops,time,summaryName,adaptive,rounds,printErrors,intervalUtilityComputation,learningRate,hiddenNodes,utilityTime,train,network,updateStart);
			break;
		}
	}
	
	private static void executeLocationBasedExperiments(String approach, int requests, String configFile, String parametersFile, String experimentalSetupFileBase, String initialParametersFile, String pathToData,
			String pathToResults, String templateFile, String actionsFile, String simrunner,int mode, int length, int hops, 
			String time, String summaryName, int adaptive, int rounds, int printErrors, int intervalUtilityComputation, int utilityTime){
		try {
			System.out.println("Starting experiments...");	
			simrunner = simrunner + "simrunner-test.jar";
			System.out.println("Simrunner: " + simrunner);
			System.out.println("Loading initial parameters...");
			Map<String,String> initialParameters = readInitialParametersFile(initialParametersFile);
			int initialServices = Integer.parseInt(initialParameters.get("services"));
			int initialDensity = Integer.parseInt(initialParameters.get("density"));
			int initialDistance = Integer.parseInt(initialParameters.get("distance"));
			int initialGateways = Integer.parseInt(initialParameters.get("gateways"));
			int initialLength = Integer.parseInt(initialParameters.get("length"));
			Map<String,String> updatedParameters = new HashMap<String,String>();
			String actionsFileBase = actionsFile;
			boolean firstTime = true;
			for (int gateways=500; gateways>=100; gateways = gateways - 200) {
				if(firstTime)
					gateways = initialGateways;
				updatedParameters.put("gateways", ""+gateways);
				for(int round = 0; round<rounds;round++){
					int roundBefore = round;
					for(int len=2;len<=5;len++) {
						if(firstTime)
							len = initialLength;
						updatedParameters.put("length", ""+len);
						for(int distance=100; distance<=100; distance=distance+25) {
							if(firstTime)
								distance = initialDistance;
							updatedParameters.put("distance", ""+distance);
							for(int services=20000; services<=100000; services=services+20000) {
								if(firstTime)
									services = initialServices;
								updatedParameters.put("services", ""+services);
								for(int density=60; density<=60; density=density+40){
									if(firstTime)
										density = initialDensity;
									updatedParameters.put("density", ""+density);
									writeInitialParametersFile(initialParametersFile,updatedParameters);
									actionsFile = actionsFileBase.replace(".dat", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+".dat");
									firstTime = false;
									String experimentalSetupFile =  "mode_"+mode+"_length_"+len+"_hops_"+hops+"_services_"+services+"_density_"+density+"_distance_"+distance+"_gateways_"+gateways+"_adaptive_"+adaptive+".dat";
									String utilityFileName = experimentalSetupFile.replace(".dat", ".xlsx");
									int rp = getRound(pathToResults+"utility-location/"+utilityFileName);
									if(rp>=10)
										break;
									else 
										round = rp;
									System.out.println("Round " + (round+1) +"...");
									switch(mode) {
									case 1:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*1),(gateways*0),approach,mode,len,hops,adaptive,0,0,"none",0);
										break;
									case 2:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(int)(gateways*0.5),(int)(gateways*0.5),approach,mode,len,hops,adaptive,0,0,"none",0);
										break;
									case 3:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*0),(gateways*1),approach,mode,len,hops,adaptive,0,0,"none",0);
										break;
									}
									BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
									System.out.println("Experiment "+experimentalSetupFile);
									out.write("approach="+approach); out.newLine();
									out.write("mode="+mode); out.newLine();
									out.write("summaryName="+summaryName); out.newLine();
									out.write("parametersFile="+experimentalSetupFile); out.newLine();
									out.write("distributionType=1"); out.newLine();
									out.write("pathToData="+pathToData); out.newLine();
									out.write("hopsLimitGatewaysAdvertisement="+hops); out.newLine();
									out.write("hopsLimitServiceAdvertisement="+hops); out.newLine();
									out.write("hopsLimitServiceRequest="+hops); out.newLine();
									out.write("distanceToUpdateRoutingTables=100"); out.newLine();
									out.write("distanceToRecognisePlaces=100"); out.newLine();
									out.write("checkGatewaysInterval=30m"); out.newLine();
									out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
									out.write("distanceToAddGateway="+distance); out.newLine();
									out.write("numberOfServices="+services); out.newLine();
									out.write("distanceToRegisterService="+distance); out.newLine();
									out.write("numberOfGateways="+gateways); out.newLine();
									out.write("pathToResults="+pathToResults); out.newLine();
									out.write("servicesAmount=10"); out.newLine();
									out.write("registrationInterval=5m"); out.newLine();
									out.write("serviceDensity="+density); out.newLine();
									out.write("numberOfRequests="+requests); out.newLine();
									out.write("discoveryInterval=5m"); out.newLine();
									out.write("discoveryType=4"); out.newLine();
									out.write("similarityThreshold=10"); out.newLine();
									out.write("topK=5"); out.newLine();
									out.write("length="+len); out.newLine();
									out.write("feedbackThreshold=10"); out.newLine();
									out.write("functionalThreshold=10"); out.newLine();
									out.write("gatewaysToAdvertise=5"); out.newLine();
									out.write("bothRoutingTables=1"); out.newLine();
									out.write("adaptiveMode="+adaptive); out.newLine();
									out.write("intervalUtilityComputation="+intervalUtilityComputation+"m"); out.newLine();
									out.write("requestsType=1"); out.newLine();
									out.write("gatewaySelection=3"); out.newLine();
									out.write("utilityFileName="+utilityFileName); out.newLine();
									out.write("round="+(round+1)); out.newLine();
									out.write("roundsLimit="+rounds); out.newLine();
									out.write("utilityTime="+utilityTime); out.newLine();
									out.close();
									String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
									for (int i = 0; i < lines.length;i++) { 
										if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
											lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
										}
									}
									out = new BufferedWriter(new FileWriter(actionsFile,false));
									for (int i = 0; i < lines.length; i++) {
										out.write(lines[i]); out.newLine(); 
									} 
									out.close();
									String s = null;
									String e = null;
									String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
									Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , newConfigFile});
									BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
									while ((s = stdInput.readLine()) != null) {
										System.out.println(s);
									}
									if(printErrors==1){
										BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
										while ((e = stdError.readLine()) != null) {
											System.out.println(e);
										}
									}
									removeFile(experimentalSetupFileBase+experimentalSetupFile);
									removeFile(newConfigFile);
									String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
									removeFile(newParametersFile);
								}
							}
						}
					}
					round = roundBefore;
				}
			}
			System.out.println("Experiments Finished.");
		}catch(Exception ex){
				ex.printStackTrace();
		}
	}

	private static void executeDomainBasedExperiments(String approach, int requests, String configFile, String parametersFile, String experimentalSetupFileBase, String initialParametersFile, String pathToData,
			String pathToResults, String templateFile, String actionsFile, String simrunner,int mode, int length,int hops,
			String time, String summaryName,int adaptive, int rounds, int printErrors, int intervalUtilityComputation, int utilityTime) {
		try {
			System.out.println("Starting experiments...");
			simrunner = simrunner + "simrunner-test.jar";
			System.out.println("Simrunner: " + simrunner);
			System.out.println("Loading initial parameters...");
			Map<String,String> initialParameters = readInitialParametersFile(initialParametersFile);
			int initialServices = Integer.parseInt(initialParameters.get("services"));
			int initialDensity = Integer.parseInt(initialParameters.get("density"));
			int initialDomains = Integer.parseInt(initialParameters.get("domains"));
			int initialGateways = Integer.parseInt(initialParameters.get("gateways"));
			int initialLength = Integer.parseInt(initialParameters.get("length"));
			Map<String,String> updatedParameters = new HashMap<String,String>(); 
			String actionsFileBase = actionsFile;
			boolean firstTime = true;			
			for (int gateways=500; gateways>=100; gateways = gateways - 200) {
				if(firstTime)
					gateways = initialGateways;
				updatedParameters.put("gateways", ""+gateways);
				for(int round=0; round<rounds; round++){
					int roundBefore = round;
					for(int len=2;len<=5;len++) {
						if(firstTime)
							len = initialLength;
						updatedParameters.put("length", ""+len);
						for(int domains=5; domains<=5; domains=domains+2) {
							if(firstTime)
								domains = initialDomains;
							updatedParameters.put("domains", ""+domains);
							for(int services=20000; services<=100000; services=services+20000) {
								if(firstTime)
									services = initialServices;
								updatedParameters.put("services", ""+services);
								for(int density=60; density<=60; density=density+40){
									if(firstTime)
										density = initialDensity;
									updatedParameters.put("density", ""+density);
									writeInitialParametersFile(initialParametersFile,updatedParameters);
									actionsFile = actionsFileBase.replace(".dat", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+".dat");
									firstTime = false;
									String experimentalSetupFile = "mode_"+mode+"_length_"+len+"_hops_"+hops+"_services_"+services+"_density_"+density+"_domains_"+domains+"_gateways_"+gateways+"_adaptive_"+adaptive+".dat";
									String utilityFileName = experimentalSetupFile.replace(".dat", ".xlsx");
									int rp = getRound(pathToResults+"utility-domain/"+utilityFileName);
									if(rp>=10)
										break;
									else 
										round = rp;
									System.out.println("Round " + (round+1) +"...");
									switch(mode) {
									case 1:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*1),(gateways*0),approach,mode,len,hops,adaptive,0,0,"none",0);
										break;
									case 2:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(int)(gateways*0.5),(int)(gateways*0.5),approach, mode,len,hops,adaptive,0,0,"none",0);
										break;
									case 3:
										updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*0),(gateways*1),approach,mode,len,hops,adaptive,0,0,"none",0);
										break;
									}
									BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
									System.out.println("Experiment "+experimentalSetupFile);
									out.write("approach="+approach); out.newLine();
									out.write("mode="+mode); out.newLine();
									out.write("summaryName="+summaryName); out.newLine();
									out.write("parametersFile="+experimentalSetupFile); out.newLine();
									out.write("distributionType=2"); out.newLine();
									out.write("pathToData="+pathToData); out.newLine();
									out.write("hopsLimitGatewaysAdvertisement="+hops); out.newLine();
									out.write("hopsLimitServiceAdvertisement="+hops); out.newLine();
									out.write("hopsLimitServiceRequest="+hops); out.newLine();
									out.write("distanceToAddGateway=100"); out.newLine();
									out.write("distanceToUpdateRoutingTables=100"); out.newLine();
									out.write("distanceToRecognisePlaces=100"); out.newLine();
									out.write("checkGatewaysInterval=30m"); out.newLine();
									out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
									out.write("numberOfServices="+services); out.newLine();
									out.write("numberOfDomains="+domains); out.newLine();
									out.write("numberOfGateways="+gateways); out.newLine();
									out.write("pathToResults="+pathToResults); out.newLine();
									out.write("servicesAmount=10"); out.newLine();
									out.write("registrationInterval=5m"); out.newLine();
									out.write("serviceDensity="+density); out.newLine();
									out.write("numberOfRequests="+requests); out.newLine();
									out.write("discoveryInterval=5m"); out.newLine();
									out.write("discoveryType=4"); out.newLine();
									out.write("similarityThreshold=10"); out.newLine();
									out.write("topK=5"); out.newLine();
									out.write("length="+len); out.newLine();
									out.write("feedbackThreshold=10"); out.newLine();
									out.write("functionalThreshold=10"); out.newLine();
									out.write("gatewaysToAdvertise=3"); out.newLine();
									out.write("bothRoutingTables=1"); out.newLine();
									out.write("superDomains=2"); out.newLine();
									out.write("adaptiveMode="+adaptive); out.newLine();
									out.write("intervalUtilityComputation="+intervalUtilityComputation+"m"); out.newLine();
									out.write("requestsType=1"); out.newLine();
									out.write("gatewaySelection=3"); out.newLine();
									out.write("utilityFileName="+utilityFileName); out.newLine();
									out.write("round="+(round+1)); out.newLine();
									out.write("roundsLimit="+rounds); out.newLine();
									out.write("utilityTime="+utilityTime); out.newLine();
									out.close();
									String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
									for (int i = 0; i < lines.length;i++) { 
										if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
											lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
										}
									}
									out = new BufferedWriter(new FileWriter(actionsFile,false));
									for (int i = 0; i < lines.length; i++) {
										out.write(lines[i]); out.newLine(); 
									} 
									out.close();
									String s = null;
									String e = null;
									String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
									Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , newConfigFile});
									BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
									while ((s = stdInput.readLine()) != null) {
										System.out.println(s);
									}
									if(printErrors==1){
										BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
										while ((e = stdError.readLine()) != null) {
											System.out.println(e);
										}
									}
									removeFile(experimentalSetupFileBase+experimentalSetupFile);
									removeFile(newConfigFile);
									String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
									removeFile(newParametersFile);
								}
							}
						}
					}
					round = roundBefore;
				}
			}
			System.out.println("Experiments Finished.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private static void executeUrbanBasedExperiments(String approach, int requests, String configFile, String parametersFile, String experimentalSetupFileBase,String initialParametersFile, String pathToData,
			String pathToResults, String templateFile, String actionsFile, String simrunner,int mode, int length,int hops, 
			String time, String summaryName, int adaptive, int rounds, int printErrors, int intervalUtilityComputation, int utilityTime) {
		try {
			System.out.println("Starting experiments...");
			simrunner = simrunner + "simrunner-test.jar";
			System.out.println("Simrunner: " + simrunner);
			System.out.println("Loading initial parameters...");
			Map<String,String> initialParameters = readInitialParametersFile(initialParametersFile);
			int initialServices = Integer.parseInt(initialParameters.get("services"));
			int initialDensity = Integer.parseInt(initialParameters.get("density"));
			int initialDistance = Integer.parseInt(initialParameters.get("distance"));
			int initialGateways = Integer.parseInt(initialParameters.get("gateways"));
			int initialThreshold = Integer.parseInt(initialParameters.get("threshold"));
			int initialLength = Integer.parseInt(initialParameters.get("length"));
			Map<String,String> updatedParameters = new HashMap<String,String>(); 
			String actionsFileBase = actionsFile;
			boolean firstTime = true;
			for (int gateways=500; gateways>=100; gateways = gateways - 200) {
				if(firstTime)
					gateways = initialGateways;
				updatedParameters.put("gateways", ""+gateways);
				for(int round=0; round<rounds; round++){
					int roundBefore = round;
					for(int len=2;len<=5;len++) {
						if(firstTime)
							len = initialLength;
						updatedParameters.put("length", ""+len);
						for(int distance=100; distance<=100; distance=distance+25) {
							if(firstTime)
								distance = initialDistance;
							updatedParameters.put("distance", ""+distance);
							for(int threshold=6; threshold<=6; threshold=threshold+2) {
								if(firstTime)
									threshold = initialThreshold;
								updatedParameters.put("threshold", ""+threshold);
								for(int services=20000; services<=100000; services=services+20000) {
									if(firstTime)
										services = initialServices;
									updatedParameters.put("services", ""+services);
									for(int density=60; density<=60; density=density+40){
										if(firstTime)
											density = initialDensity;
										updatedParameters.put("density", ""+density);
										writeInitialParametersFile(initialParametersFile,updatedParameters);
										actionsFile = actionsFileBase.replace(".dat", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+".dat");
										firstTime = false;
										String experimentalSetupFile = "mode_"+mode+"_length_"+len+"_hops_"+hops+"_services_"+services+"_density_"+density+"_distance_"+distance+"_threshold_"+threshold+"_gateways_"+gateways+"_adaptive_"+adaptive+".dat";
										String utilityFileName = experimentalSetupFile.replace(".dat", ".xlsx");
										int rp = getRound(pathToResults+"utility-urban/"+utilityFileName);
										if(rp>=10)
											break;
										else 
											round = rp;
										System.out.println("Round " + (round+1) +"...");
										switch(mode) {
										case 1:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*1),(gateways*0),approach,mode,len,hops,adaptive,0,0,"none",0);
											break;
										case 2:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(int)(gateways*0.5),(int)(gateways*0.5),approach,mode,len,hops,adaptive,0,0,"none",0);
											break;
										case 3:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*0),(gateways*1),approach,mode,len,hops,adaptive,0,0,"none",0);
											break;
										}
										BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
										System.out.println("Experiment "+experimentalSetupFile);
										out.write("approach="+approach); out.newLine();
										out.write("mode="+mode); out.newLine();
										out.write("summaryName="+summaryName); out.newLine();
										out.write("parametersFile="+experimentalSetupFile); out.newLine();
										out.write("distributionType=3"); out.newLine();
										out.write("pathToData="+pathToData); out.newLine();
										out.write("hopsLimitGatewaysAdvertisement="+hops); out.newLine();
										out.write("hopsLimitServiceAdvertisement="+hops); out.newLine();
										out.write("hopsLimitServiceRequest="+hops); out.newLine();
										out.write("distanceToUpdateRoutingTables=100"); out.newLine();
										out.write("distanceToRecognisePlaces="+distance); out.newLine();
										out.write("checkGatewaysInterval=30m"); out.newLine();
										out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
										out.write("numberOfServices="+services); out.newLine();
										out.write("numberOfGateways="+gateways); out.newLine();
										out.write("pathToResults="+pathToResults); out.newLine();
										out.write("servicesAmount=10"); out.newLine();
										out.write("registrationInterval=5m"); out.newLine();
										out.write("numberOfRequests="+requests); out.newLine();
										out.write("serviceDensity="+density); out.newLine();
										out.write("discoveryInterval=30m"); out.newLine();
										out.write("discoveryType=4"); out.newLine();
										out.write("similarityThreshold=5"); out.newLine();
										out.write("topK=5"); out.newLine();
										out.write("length="+len); out.newLine();
										out.write("feedbackThreshold=10"); out.newLine();
										out.write("functionalThreshold=10"); out.newLine();
										out.write("similarityThresholdGateways="+threshold); out.newLine();
										out.write("similarityThresholdRegistration="+threshold); out.newLine();
										out.write("similarityThresholdRequest="+threshold); out.newLine();
										out.write("topSimilarGateways=10"); out.newLine();
										out.write("gatewaysToAdvertise=5"); out.newLine();
										out.write("bothRoutingTables=1"); out.newLine();
										out.write("superDomains=2"); out.newLine();
										out.write("adaptiveMode="+adaptive); out.newLine();
										out.write("adaptiveThreshold=8"); out.newLine();
										out.write("intervalUtilityComputation="+intervalUtilityComputation+"m"); out.newLine();
										out.write("requestsType=1"); out.newLine();
										out.write("gatewaySelection=3"); out.newLine();
										out.write("utilityFileName="+utilityFileName); out.newLine();
										out.write("round="+(round+1)); out.newLine();
										out.write("roundsLimit="+rounds); out.newLine();
										out.write("utilityTime="+utilityTime); out.newLine();
										out.close();
										String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
										for (int i = 0; i < lines.length;i++) { 
											if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
												lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
											}
										}
										out = new BufferedWriter(new FileWriter(actionsFile,false));
										for (int i = 0; i < lines.length; i++) {
											out.write(lines[i]); out.newLine(); 
										} 
										out.close();
										String s = null;
										String e = null;
										String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
										Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , newConfigFile});
										BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
										while ((s = stdInput.readLine()) != null) {
											System.out.println(s);
										}
										if(printErrors==1){
											BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
											while ((e = stdError.readLine()) != null) {
												System.out.println(e);
											}
										}
										removeFile(experimentalSetupFileBase+experimentalSetupFile);
										removeFile(newConfigFile);
										String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_0_0.0_none_0.xml");
										removeFile(newParametersFile);
									}
								}
							}
						}
					}
					round = roundBefore;
				}
			}
			System.out.println("Experiments Finished.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static void executeUrbanBasedAdaptiveExperiments(String approach, int requests, String configFile, String parametersFile, String experimentalSetupFileBase, String initialParametersFile,
			String pathToData, String pathToResults, String templateFile, String actionsFile, String simrunner,int mode, int length,
			int hops, String time, String summaryName,int adaptive, int rounds, int printErrors, int intervalUtilityComputation, 
			double learningRate, int hiddenNodes, int utilityTime, int train, String network, int updateStart) {
		try {
			System.out.println("Starting experiments...");
			simrunner = simrunner + "simrunner-test.jar";
			System.out.println("Simrunner: " + simrunner);
			System.out.println("Loading initial parameters...");
			Map<String,String> initialParameters = readInitialParametersFile(initialParametersFile);
			int initialServices = Integer.parseInt(initialParameters.get("services"));
			int initialDensity = Integer.parseInt(initialParameters.get("density"));
			int initialDistance = Integer.parseInt(initialParameters.get("distance"));
			int initialGateways = Integer.parseInt(initialParameters.get("gateways"));
			int initialThreshold = Integer.parseInt(initialParameters.get("threshold"));
			int initialLength = Integer.parseInt(initialParameters.get("length"));
			Map<String,String> updatedParameters = new HashMap<String,String>(); 
			String actionsFileBase = actionsFile;
			boolean firstTime = true;
			for (int gateways=500; gateways>=100; gateways = gateways - 200) {
				if(firstTime)
					gateways = initialGateways;
				updatedParameters.put("gateways", ""+gateways);
				for(int round=0; round<rounds; round++){
					int roundBefore = round;
					for(int len=2;len<=5;len++) {
						if(firstTime)
							len = initialLength;
						updatedParameters.put("length", ""+len);
						for(int distance=100; distance<=100; distance=distance+25) {
							if(firstTime)
								distance = initialDistance;
							updatedParameters.put("distance", ""+distance);
							for(int threshold=6; threshold<=6; threshold=threshold+2) {
								if(firstTime)
									threshold = initialThreshold;
								updatedParameters.put("threshold", ""+threshold);
								for(int services=20000; services<=100000; services=services+20000) {
									if(firstTime)
										services = initialServices;
									updatedParameters.put("services", ""+services);
									for(int density=60; density<=60; density=density+20){
										if(firstTime)
											density = initialDensity;
										updatedParameters.put("density", ""+density);
										writeInitialParametersFile(initialParametersFile,updatedParameters);
										actionsFile = actionsFileBase.replace(".dat", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_"+hiddenNodes+"_"+learningRate+"_"+network+"_"+train+".dat");
										firstTime = false;
										String experimentalSetupFile = "mode_"+mode+"_length_"+len+"_hops_"+hops+"_services_"+services+"_density_"+density+"_distance_"+distance+"_threshold_"+threshold+"_gateways_"+gateways+"_adaptive_"+adaptive+".dat";
										String utilityFileName = experimentalSetupFile.replace(".dat", ".xlsx");
										int rp = getRound(pathToResults+"utility-adaptive/"+utilityFileName);
										if(rp>=10)
											break;
										else 
											round = rp;
										System.out.println("Round " + (round+1) +"...");
										switch(mode) {
										case 1:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*1),(gateways*0),approach,mode,len,hops,adaptive,hiddenNodes,learningRate,network,train);
											break;
										case 2:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(int)(gateways*0.5),(int)(gateways*0.5),approach,mode,len,hops,adaptive,hiddenNodes,learningRate,network,train);
											break;
										case 3:
											updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*0),(gateways*1),approach,mode,len,hops,adaptive,hiddenNodes,learningRate,network,train);
											break;
										}
										BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
										System.out.println("Experiment "+experimentalSetupFile);
										out.write("approach="+approach); out.newLine();
										out.write("mode="+mode); out.newLine();
										out.write("summaryName="+summaryName); out.newLine();
										out.write("parametersFile="+experimentalSetupFile); out.newLine();
										out.write("distributionType=4"); out.newLine();
										out.write("pathToData="+pathToData); out.newLine();
										out.write("hopsLimitGatewaysAdvertisement="+hops); out.newLine();
										out.write("hopsLimitServiceAdvertisement="+hops); out.newLine();
										out.write("hopsLimitServiceRequest="+hops); out.newLine();
										out.write("distanceToUpdateRoutingTables=100"); out.newLine();
										out.write("distanceToRecognisePlaces="+distance); out.newLine();
										out.write("checkGatewaysInterval=30m"); out.newLine();
										out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
										out.write("numberOfServices="+services); out.newLine();
										out.write("numberOfGateways="+gateways); out.newLine();
										out.write("pathToResults="+pathToResults); out.newLine();
										out.write("servicesAmount=10"); out.newLine();
										out.write("registrationInterval=5m"); out.newLine();
										out.write("numberOfRequests="+requests); out.newLine();
										out.write("serviceDensity="+density); out.newLine();
										out.write("discoveryInterval=5m"); out.newLine();
										out.write("discoveryType=4"); out.newLine();
										out.write("similarityThreshold=5"); out.newLine();
										out.write("topK=5"); out.newLine();
										out.write("length="+len); out.newLine();
										out.write("feedbackThreshold=10"); out.newLine();
										out.write("functionalThreshold=10"); out.newLine();
										out.write("similarityThresholdGateways="+threshold); out.newLine();
										out.write("similarityThresholdRegistration="+threshold); out.newLine();
										out.write("similarityThresholdRequest="+threshold); out.newLine();
										out.write("topSimilarGateways=10"); out.newLine();
										out.write("gatewaysToAdvertise=5"); out.newLine();
										out.write("bothRoutingTables=1"); out.newLine();
										out.write("superDomains=2"); out.newLine();
										out.write("adaptiveMode="+adaptive); out.newLine();
										out.write("adaptiveThreshold=8"); out.newLine();
										out.write("intervalUtilityComputation="+intervalUtilityComputation+"m"); out.newLine();
										out.write("requestsType=1"); out.newLine();
										out.write("gatewaySelection=3"); out.newLine();
										out.write("utilityFileName="+utilityFileName); out.newLine();
										out.write("round="+(round+1)); out.newLine();
										out.write("roundsLimit="+rounds); out.newLine();
										out.write("hiddenNodes="+hiddenNodes); out.newLine();
										out.write("learningRate="+learningRate); out.newLine();
										out.write("utilityTime="+utilityTime); out.newLine();
										out.write("train="+train); out.newLine();
										out.write("network="+network); out.newLine();
										out.write("updateStart="+updateStart); out.newLine();
										out.close();
										String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
										for (int i = 0; i < lines.length;i++) { 
											if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
												lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
											}
										}
										out = new BufferedWriter(new FileWriter(actionsFile,false));
										for (int i = 0; i < lines.length; i++) {
											out.write(lines[i]); out.newLine(); 
										} 
										out.close();
										String s = null;
										String e = null;
										String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_"+hiddenNodes+"_"+learningRate+"_"+network+"_"+train+".xml");
										Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , newConfigFile});
										BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
										while ((s = stdInput.readLine()) != null) {
											System.out.println(s);
										}
										if(printErrors==1){
											BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
											while ((e = stdError.readLine()) != null) {
												System.out.println(e);
											}
										}
										removeFile(experimentalSetupFileBase+experimentalSetupFile);
										removeFile(newConfigFile);
										String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+len+"_"+hops+"_"+adaptive+"_"+hiddenNodes+"_"+learningRate+"_"+network+"_"+train+".xml");
										removeFile(newParametersFile);
									}
								}
							}
						}
					}
					round = roundBefore;
				}
			}
			System.out.println("Experiments Finished.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private static void updateParametersFiles(String configFile, String parametersFile, String finishedTime, String actionsFile, int staticGateways, 
			int mobileGateways, String approach, int mode, int length,int hops, int adaptive, int hiddenNodes, double learningRate, 
			String network, int train) {
		System.out.println("Setting paremeters...");
		try{
			double minSpeed = 10/3.6;
			minSpeed = minSpeed * 10;
			minSpeed = Math.floor(minSpeed);
			minSpeed = minSpeed/10;
			double maxSpeed = 50/3.6;
			maxSpeed = maxSpeed * 10;
			maxSpeed = Math.floor(maxSpeed);
			maxSpeed = maxSpeed/10;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder(); 
			org.w3c.dom.Document documentParameters =db.parse(parametersFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("Variable").getLength(); i++) {
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("seed")){
					int seed = (int) (Math.random() * 1000);
					if(seed == 0)
						seed = 250;
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + seed);
				}
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+finishedTime);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("actions"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(actionsFile);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-X"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2000");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-Y"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2000");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("MOVEMENT"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("SOCIAL");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+minSpeed);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+maxSpeed);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN_CITIZEN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX_CITIZEN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
			}
			TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
			Transformer transformerTime = transformerFactoryTime.newTransformer(); 
			DOMSource sourceTime = new DOMSource(documentParameters); 
			String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+length+"_"+hops+"_"+adaptive+"_"+hiddenNodes+"_"+learningRate+"_"+network+"_"+train+".xml");
			StreamResult resultTime = new StreamResult(new File(newParametersFile));
			transformerTime.transform(sourceTime, resultTime);
			
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder(); 
			documentParameters =db.parse(configFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("xi:include").getLength(); i++) {
				if(documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("eval_parameters"))
					documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("href").setNodeValue(newParametersFile);
			}
			transformerFactoryTime = TransformerFactory.newInstance(); 
			transformerTime = transformerFactoryTime.newTransformer(); 
			sourceTime = new DOMSource(documentParameters); 
			String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+length+"_"+hops+"_"+adaptive+"_"+hiddenNodes+"_"+learningRate+"_"+network+"_"+train+".xml");
			resultTime = new StreamResult(new File(newConfigFile));
			transformerTime.transform(sourceTime, resultTime);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static Map<String, String> readInitialParametersFile(String initialParametersFile) {
		HashMap<String,String> parameters = new HashMap<String,String>();
		try {
			String[] lines = Files.readAllLines(new File(initialParametersFile).toPath()).toArray(new String[0]);
			for (int i = 0; i < lines.length;i++) { 
				String key = lines[i].split("=")[0];
				String value = lines[i].split("=")[1];
				parameters.put(key, value);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return parameters;
	}
	
	private static void createInitialParametersFile(String approach, String initialParametersFile) {
		Map<String,String> parameters = new HashMap<String,String>();
		switch(approach){
			case "location":
				parameters.put("services", "20000");
				parameters.put("density", "60");
				parameters.put("distance", "100");
				parameters.put("gateways", "500");
				parameters.put("length", "2");
				break;
			case "domain":
				parameters.put("services", "20000");
				parameters.put("density", "60");
				parameters.put("domains", "5");
				parameters.put("gateways", "500");
				parameters.put("length", "2");
				break;
			case "urban":
				parameters.put("services", "20000");
				parameters.put("density", "60");
				parameters.put("threshold", "6");
				parameters.put("distance", "100");
				parameters.put("gateways", "500");
				parameters.put("length", "2");
				break;
			case "adaptive":
				parameters.put("services", "20000");
				parameters.put("density", "60");
				parameters.put("threshold", "6");
				parameters.put("distance", "100");
				parameters.put("gateways", "500");
				parameters.put("length", "2");
				break;
		}
		writeInitialParametersFile(initialParametersFile,parameters);
	}
	
	
	private static void writeInitialParametersFile(String initialParametersFile,
			Map<String, String> parameters) {
		try {
			File file = new File(initialParametersFile);
			file.getParentFile().mkdirs();
			file.createNewFile();
			FileWriter fw = new FileWriter(file,false);
			BufferedWriter out = new BufferedWriter(fw);
			parameters.forEach((key,value)->{
				try {
					out.write(key+"="+value); out.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			out.close();
			fw.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void removeFile(String fileName) {
		File file = new File(fileName);
		file.delete();
	}
	
	private static void createSummaryFile(String approach, String pathToResults, String summaryName){
		try{
			File file = new File(pathToResults + "/" + summaryName);
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
				Workbook generalSummary = new XSSFWorkbook();
				Sheet messagesStatic = generalSummary.createSheet("messagesStatic");
				Row headerRow = messagesStatic.createRow(0);
				Cell cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				int c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-adp");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-adp");

				Sheet messagesMobile = generalSummary.createSheet("messagesMobile");
				headerRow = messagesMobile.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-adp");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-adp");

				Sheet messagesBoth = generalSummary.createSheet("messagesBoth");
				headerRow = messagesBoth.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-config");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-reg");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-dis");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean-adp");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd-adp");

				Sheet solvedRequests = generalSummary.createSheet("solvedRequests");
				headerRow = solvedRequests.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("totalRequests");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("solvedRequests");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("notSolvedRequests");

				Sheet hops = generalSummary.createSheet("hops");
				headerRow = hops.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("min");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q1");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("median");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q2");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("max");

				Sheet precision = generalSummary.createSheet("precision");
				headerRow = precision.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("min");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q1");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("median");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q2");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("max");

				Sheet time = generalSummary.createSheet("time");
				headerRow = time.createRow(0);
				cell = headerRow.createCell(0);
				cell.setCellValue("approach");
				cell = headerRow.createCell(1);
				cell.setCellValue("mode");
				cell = headerRow.createCell(2);
				cell.setCellValue("gateways");
				c = 3;
				switch(approach){
				case "location":
					cell = headerRow.createCell(c);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(c);
					cell.setCellValue("services");
					break;
				case "domain":
					cell = headerRow.createCell(3);
					cell.setCellValue("domains");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("services");
					break;
				case "urban":
					cell = headerRow.createCell(3);
					cell.setCellValue("threshold");
					c++;
					cell = headerRow.createCell(4);
					cell.setCellValue("distance");
					c++;
					cell = headerRow.createCell(5);
					cell.setCellValue("density");
					c++;
					cell = headerRow.createCell(6);
					cell.setCellValue("services");
					break;
				}
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("hops");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("length");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("mean");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("sd");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("min");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q1");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("median");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("q2");
				c++;
				cell = headerRow.createCell(c);
				cell.setCellValue("max");

				FileOutputStream fileOut = new FileOutputStream(file);
				generalSummary.write(fileOut);
				fileOut.close();
				generalSummary.close();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("resource")
	private static int getRound(String fileName) {
		int round=0;
		File file = new File(fileName);
		if(file.exists()){
			try {
				Workbook utilityFile = new XSSFWorkbook(file);
				if(utilityFile.getNumberOfSheets()>4) {
					round=utilityFile.getNumberOfSheets()-4;
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return round;
	}

}
