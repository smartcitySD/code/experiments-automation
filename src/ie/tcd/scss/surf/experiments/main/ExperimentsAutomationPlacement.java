package ie.tcd.scss.surf.experiments.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class ExperimentsAutomationPlacement {
	
	private static String simrunnerFile = "";
	private static String configFile = "";
	private static String parametersFile = "";
	private static String pathToActions = "";
	private static String pathToCoordinates = "";
	private static String pathToMatrices = "";
	private static String pathToResults = "";
	private static int printErrors = 0;

	public static void main(String[] args) {
		String approach = args[0];
		int scenario = Integer.parseInt(args[1]);
		int servers = Integer.parseInt(args[2]); 
		String base = args[3];
		int read = Integer.parseInt(args[4]);
		printErrors = Integer.parseInt(args[5]);
		int iterations = 0;
		int ants = 0;
		
		simrunnerFile = base + "Repositories/Simonstrator/simonstrator-simrunner/simrunner-placement.jar";
		configFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/mec/mec_default.xml";
		parametersFile = base + "Repositories/Simonstrator/simonstrator-simrunner/config/mec/mec_eval_parameters.xml";
		pathToActions = base + "Repositories/Simonstrator/simonstrator-simrunner/config/mec/actions/actions.dat";
		pathToCoordinates = base + "Repositories/Simonstrator/simonstrator-simrunner/config/mec/links_coordinates.json";
		pathToMatrices = base + "Repositories/Simonstrator/simonstrator-simrunner//config/mec/trainning/";
		pathToResults = base + "Repositories/SmartCitySD/Results/service-placement-laptop/";
		
		String fileNameStatus = base + "Repositories/Simonstrator/simonstrator-simrunner/config/mec/initial/initial_"+ approach + "_" + scenario + "_" + servers;
		HashMap<String,String> experimentStatus = readExperimentsStatus(fileNameStatus);
					
		switch(approach) {
			case "maaco1":
				iterations = 100;
				if(read == 1)
					iterations = Integer.parseInt(experimentStatus.get("iterations"));
				while(iterations <= 10000) {
					ants = 10;
					if(read == 1)
						ants = Integer.parseInt(experimentStatus.get("ants"));
					while(ants <= 1000) {
						int functions = 1;
						if(read == 1)
							functions = Integer.parseInt(experimentStatus.get("functions"));
						while(functions <= 5) {
							runACOBasedApproach("MAACO", scenario, servers, functions, iterations, ants, "BC");
							experimentStatus.put("functions", "" + functions);
							experimentStatus.put("iterations", "" + iterations);
							experimentStatus.put("ants", "" + ants);
							writeExperimentStatus(fileNameStatus, experimentStatus);
							functions = functions + 1;
							if(read == 1)
								read = 0;
						}
						ants = ants * 10;
					}	
					iterations = iterations * 10;
				}
				break;
			case "maaco2":
				iterations = 100;
				if(read == 1)
					iterations = Integer.parseInt(experimentStatus.get("iterations"));
				while(iterations <= 10000) {
					ants = 10;
					if(read == 1)
						ants = Integer.parseInt(experimentStatus.get("ants"));
					while(ants <= 1000) {
						int functions = 1;
						if(read == 1)
							functions = Integer.parseInt(experimentStatus.get("functions"));
						while(functions <= 5) {
							runACOBasedApproach("MAACO", scenario, servers, functions, iterations, ants, "HMM");
							experimentStatus.put("functions", "" + functions);
							experimentStatus.put("iterations", "" + iterations);
							experimentStatus.put("ants", "" + ants);
							writeExperimentStatus(fileNameStatus, experimentStatus);
							functions = functions + 1;
							if(read == 1)
								read = 0;
						}
						ants = ants * 10;
					}	
					iterations = iterations * 10;
				}
				break;
			case "aco":
				iterations = 100;
				if(read == 1)
					iterations = Integer.parseInt(experimentStatus.get("iterations"));
				while(iterations <= 100) {
					ants = 10;
					if(read == 1)
						ants = Integer.parseInt(experimentStatus.get("ants"));
					while(ants <= 10) {
						int functions = 1;
						if(read == 1)
							functions = Integer.parseInt(experimentStatus.get("functions"));
						while(functions <= 5) {
							runACOBasedApproach(approach.toUpperCase(), scenario, servers, functions, iterations, ants, "NONE");
							experimentStatus.put("functions", "" + functions);
							experimentStatus.put("iterations", "" + iterations);
							experimentStatus.put("ants", "" + ants);
							writeExperimentStatus(fileNameStatus, experimentStatus);
							functions = functions + 1;
							if(read == 1)
								read = 0;
						}
						ants = ants * 10;
					}
					iterations = iterations * 10;
				}
				break;
			case "bestfit":
			case "closestfit":
			case "ilp":
			case "maxfit":
			case "multiopt":
			case "random":
			case "sa":
				int functions = 1;
				if(read == 1)
					functions = Integer.parseInt(experimentStatus.get("functions"));
				while(functions <= 5) {
					runBaselineApproach(approach.toUpperCase(), scenario, servers, functions);
					writeExperimentStatus(fileNameStatus, experimentStatus);
					functions = functions + 1;
					if(read == 1)
						read = 0;
				}
				break;
		}			
		
	}

	private static HashMap<String,String> readExperimentsStatus(String fileNameStatus) {
		HashMap<String,String> parameters = new HashMap<String,String>();
		File file = new File(fileNameStatus);
		if(file.exists()) {
			try {
				String[] lines = Files.readAllLines(new File(fileNameStatus).toPath()).toArray(new String[0]);
				for (int i = 0; i < lines.length;i++) { 
					String key = lines[i].split("=")[0];
					String value = lines[i].split("=")[1];
					parameters.put(key, value);
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return parameters;
	}
	
	private static void writeExperimentStatus(String initialParametersFile, Map<String, String> parameters) {
		try {
			File file = new File(initialParametersFile);
			file.getParentFile().mkdirs();
			file.createNewFile();
			FileWriter fw = new FileWriter(file,false);
			BufferedWriter out = new BufferedWriter(fw);
			parameters.forEach((key,value)->{
				try {
					out.write(key+"="+value); out.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			out.close();
			fw.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void runBaselineApproach(String approach, int scenario, int servers, int functions) {
		try{
			String experiment = "*** Experiment approach " + approach;
			experiment = experiment + " :: scenario " + scenario;
			experiment = experiment + " :: servers " + servers;
			experiment = experiment + " :: functions " + functions + " ***";
			System.out.println(experiment);
			String s = null;
			String e = null;
			String[] files = updateParametersFiles(approach, scenario, servers, functions, 0, 0, "NONE");
			String newParametersFile = files[0];
			String newConfigFile = files[1];
			Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunnerFile, newConfigFile});
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}
			if(printErrors==1){
				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				while ((e = stdError.readLine()) != null) {
					System.out.println(e);
				}
			}
			removeFile(newConfigFile);
			removeFile(newParametersFile);
		}catch(Exception ex) {
			System.out.println("Problems executing approach " + approach + " ...");
			ex.printStackTrace();
		}
	}

	private static void runACOBasedApproach(String approach, int scenario, int servers, int functions, int iterations, int ants, String predictionModel) {
		try{
			String experiment = "*** Experiment approach " + approach;
			experiment = experiment + " :: scenario " + scenario;
			experiment = experiment + " :: servers " + servers;
			experiment = experiment + " :: functions " + functions;
			experiment = experiment + " :: iterations " + iterations;
			experiment = experiment + " :: ants " + ants;
			experiment = experiment + " :: prediction " + predictionModel + " ***";
			System.out.println(experiment);
			String s = null;
			String e = null;
			String[] files = updateParametersFiles(approach, scenario, servers, functions, iterations, ants, predictionModel);
			String newParametersFile = files[0];
			String newConfigFile = files[1];
			Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunnerFile , newConfigFile});
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}
			if(printErrors==1){
				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				while ((e = stdError.readLine()) != null) {
					System.out.println(e);
				}
			}
			removeFile(newConfigFile);
			removeFile(newParametersFile);
		}catch(Exception ex) {
			System.out.println("Problems executing approach " + approach + " ...");
			ex.printStackTrace();
		}
	}
	
	private static String[] updateParametersFiles(String approach, int scenario, int servers, int functions, int iterations, int ants, String predictionModel) {
		System.out.println("Setting paremeters...");
		String [] files = {"", ""}; 
		String newConfigFile = "";
		String newParametersFile = "";
		int numMecSystems = 5;
		int serversPerSystem = servers / numMecSystems;
		int genericUsers = 750;
		int operaUsers = 225;
		int ambulanceUsers = 25;
		String muServiceTime = "";
		String lambdaAmbulance = "";
		String lambdaGeneric = "";
		String lambdaOpera = "";
		
		if(scenario == 1) {
			muServiceTime = "5m";
			lambdaAmbulance = "30s";
			lambdaGeneric = "3m";
			lambdaOpera = "1m";
		}
		
		if(scenario == 2) {
			muServiceTime = "30s";
			lambdaAmbulance = "1m";
			lambdaGeneric = "3m";
			lambdaOpera = "2m";
		}
		
		if(scenario == 3) {
			muServiceTime = "2m";
			lambdaAmbulance = "2m";
			lambdaGeneric = "2m";
			lambdaOpera = "2m";
		}
		
		try{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder(); 
			org.w3c.dom.Document documentParameters =db.parse(parametersFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("Variable").getLength(); i++) {
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("seed")){
					int seed = (int) (Math.random() * 1000);
					if(seed == 0)
						seed = 250;
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + 961);
				}
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("actions"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(pathToActions);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_EDGE_SERVERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + servers);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MEC_SYSTEMS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + numMecSystems);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("EDGE_SERVERS_PER_SYSTEM"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + serversPerSystem);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_GENERIC_USERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + genericUsers);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_OPERA_USERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + operaUsers);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_AMBULANCE_USERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + ambulanceUsers);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("LINKS_COORDINATES_FILE"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(pathToCoordinates);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("RESULTS_PATH"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(pathToResults);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("MATRICES_FOLDER"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(pathToMatrices);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SCENARIO"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+scenario);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("MU_SERVICE_TIME"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(muServiceTime);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("APPROACH"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(approach);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("FUNCTIONS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+functions);				
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("LAMBDA_GENERIC_ARRIVAL"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(lambdaGeneric);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("LAMBDA_AMBULANCE_ARRIVAL"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(lambdaAmbulance);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("LAMBDA_OPERA_ARRIVAL"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(lambdaOpera);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("PREDICTION_ALGORITHM"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(predictionModel);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("ANTS_NUMBER"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+ants);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("ITERATIONS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+iterations);
			}
			TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
			Transformer transformerTime = transformerFactoryTime.newTransformer(); 
			DOMSource sourceTime = new DOMSource(documentParameters); 
			newParametersFile = "";
			if(iterations > 0)
				newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+scenario+"_"+servers+"_"+functions+"_"+predictionModel+"_"+iterations+"_"+ants+".xml");
			else
				newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+scenario+"_"+servers+"_"+functions+"_"+predictionModel+".xml");
			StreamResult resultTime = new StreamResult(new File(newParametersFile));
			transformerTime.transform(sourceTime, resultTime);
			
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder(); 
			documentParameters =db.parse(configFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("xi:include").getLength(); i++) {
				if(documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("eval_parameters"))
					documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("href").setNodeValue(newParametersFile);
			}
			transformerFactoryTime = TransformerFactory.newInstance(); 
			transformerTime = transformerFactoryTime.newTransformer(); 
			sourceTime = new DOMSource(documentParameters);
			newConfigFile = "";
			if(iterations > 0)
				newConfigFile = configFile.replace(".xml", "_"+approach+"_"+scenario+"_"+servers+"_"+functions+"_"+predictionModel+"_"+iterations+"_"+ants+".xml");
			else
				newConfigFile = configFile.replace(".xml", "_"+approach+"_"+scenario+"_"+servers+"_"+functions+"_"+predictionModel+".xml");
			resultTime = new StreamResult(new File(newConfigFile));
			transformerTime.transform(sourceTime, resultTime);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		files[0] = newParametersFile;
		files[1] = newConfigFile;
		
		return files;
	}
	
	private static void removeFile(String fileName) {
		File file = new File(fileName);
		file.delete();
	}

	public static String getConfigFile() {
		return configFile;
	}

	public static void setConfigFile(String configFile) {
		ExperimentsAutomationPlacement.configFile = configFile;
	}

	public static String getParametersFile() {
		return parametersFile;
	}

	public static void setParametersFile(String parametersFile) {
		ExperimentsAutomationPlacement.parametersFile = parametersFile;
	}

	public static String getPathToResults() {
		return pathToResults;
	}

	public static void setPathToResults(String pathToResults) {
		ExperimentsAutomationPlacement.pathToResults = pathToResults;
	}

	public static String getPathToActions() {
		return pathToActions;
	}

	public static void setPathToActions(String pathToActions) {
		ExperimentsAutomationPlacement.pathToActions = pathToActions;
	}

	public static String getPathToCoordinates() {
		return pathToCoordinates;
	}

	public static void setPathToCoordinates(String pathToCoordinates) {
		ExperimentsAutomationPlacement.pathToCoordinates = pathToCoordinates;
	}

	public static String getPathToMatrices() {
		return pathToMatrices;
	}

	public static void setPathToMatrices(String pathToMatrices) {
		ExperimentsAutomationPlacement.pathToMatrices = pathToMatrices;
	}

}
