package ie.tcd.scss.surf.experiments.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.file.Files;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SDExperimentsJointPaper {

	public static void main(String[] args) {
		
		String templateFile =  "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/template.dat";
		String actionsFile =  "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/actions.dat";
		String configFile = "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_default.xml";
		String parametersFile = "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_eval_parameters.xml";
		String simrunner = "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/simrunner.jar";
		String pathToData = "/home/ubuntu/Repositories/SmartCitySD/Data";
		String pathToResults = "/home/ubuntu/Repositories/SmartCitySD/Results/paper-cit-tcd";
		String experimentalSetupFileBase =  "/home/ubuntu/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/";
		
		/*String templateFile =  "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/template.dat";
		String actionsFile =  "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/actions.dat";
		String configFile = "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_default.xml";
		String parametersFile = "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_eval_parameters.xml";
		String simrunner = "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/simrunner-joint-paper.jar";
		String pathToData = "D://Christian/Repositories/SmartCitySD/Data";
		String pathToResults = "D://Christian/Repositories/SmartCitySD/Results/paper-cit-tcd";
		String experimentalSetupFileBase =  "D://Christian/Repositories/Simonstrator/simonstrator-simrunner/config/service-discovery/joint-paper/";*/
		
		
		
		String approach = "random";
		
		/*String base = "../../../Simonstrator/simonstrator-simrunner/config/";
		String actionsFile =  "template.dat";*/
		
		System.out.println("Setting paremeters...");
		
		try{
			File file = new File(pathToResults + "/summary.xlsx");
			file.createNewFile();
			Workbook generalSummary = new XSSFWorkbook();
			Sheet hops = generalSummary.createSheet("hops");
			Row headerRow = hops.createRow(0);
			Cell cell = headerRow.createCell(0);
			cell.setCellValue("mobility");
			cell = headerRow.createCell(1);
			cell.setCellValue("gateways");
			cell = headerRow.createCell(2);
			cell.setCellValue("alternatives");
			cell = headerRow.createCell(3);
			cell.setCellValue("services");
			cell = headerRow.createCell(4);
			cell.setCellValue("length");
			cell = headerRow.createCell(5);
			cell.setCellValue("mean");
			cell = headerRow.createCell(6);
			cell.setCellValue("sd");
			cell = headerRow.createCell(7);
			cell.setCellValue("min");
			cell = headerRow.createCell(8);
			cell.setCellValue("q1");
			cell = headerRow.createCell(9);
			cell.setCellValue("median");
			cell = headerRow.createCell(10);
			cell.setCellValue("q2");
			cell = headerRow.createCell(11);
			cell.setCellValue("max");
			
			Sheet precision = generalSummary.createSheet("precision");
			headerRow = precision.createRow(0);
			cell = headerRow.createCell(0);
			cell.setCellValue("mobility");
			cell = headerRow.createCell(1);
			cell.setCellValue("gateways");
			cell = headerRow.createCell(2);
			cell.setCellValue("alternatives");
			cell = headerRow.createCell(3);
			cell.setCellValue("services");
			cell = headerRow.createCell(4);
			cell.setCellValue("length");
			cell = headerRow.createCell(5);
			cell.setCellValue("mean");
			cell = headerRow.createCell(6);
			cell.setCellValue("sd");
			cell = headerRow.createCell(7);
			cell.setCellValue("min");
			cell = headerRow.createCell(8);
			cell.setCellValue("q1");
			cell = headerRow.createCell(9);
			cell.setCellValue("median");
			cell = headerRow.createCell(10);
			cell.setCellValue("q2");
			cell = headerRow.createCell(11);
			cell.setCellValue("max");
			
			Sheet time = generalSummary.createSheet("time");
			headerRow = time.createRow(0);
			cell = headerRow.createCell(0);
			cell.setCellValue("mobility");
			cell = headerRow.createCell(1);
			cell.setCellValue("gateways");
			cell = headerRow.createCell(2);
			cell.setCellValue("alternatives");
			cell = headerRow.createCell(3);
			cell.setCellValue("services");
			cell = headerRow.createCell(4);
			cell.setCellValue("length");
			cell = headerRow.createCell(5);
			cell.setCellValue("mean");
			cell = headerRow.createCell(6);
			cell.setCellValue("sd");
			cell = headerRow.createCell(7);
			cell.setCellValue("min");
			cell = headerRow.createCell(8);
			cell.setCellValue("q1");
			cell = headerRow.createCell(9);
			cell.setCellValue("median");
			cell = headerRow.createCell(10);
			cell.setCellValue("q2");
			cell = headerRow.createCell(11);
			cell.setCellValue("max");
			FileOutputStream fileOut = new FileOutputStream(file);
			generalSummary.write(fileOut);
			fileOut.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			
		for(int mobility=0;mobility<2;mobility++){
			for(int gateways=5;gateways>=1;gateways--){
				try{
					DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
					DocumentBuilder db = dbf.newDocumentBuilder(); 
					org.w3c.dom.Document documentParameters =db.parse(parametersFile);
					for (int i = 0; i <	  documentParameters.getElementsByTagName("Variable").getLength(); i++) {
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("seed")){
							int seed = (int) (Math.random() * 1000);
							if(seed == 0)
								seed = 250;
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + seed);
						}
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishedTme"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+1300);
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("actions"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(actionsFile);
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-X"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("10");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-Y"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("10");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+gateways);
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("MOVEMENT"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("GAUSS");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN")){
							if(mobility==0)
								documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0.0");
							else
								documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
						}
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX")){
							if(mobility==0)
								documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0.0");
							else
								documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2.5");
						}
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN_CITIZEN"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
						if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX_CITIZEN"))
							documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2.5");
					}
					TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
					Transformer transformerTime = transformerFactoryTime.newTransformer(); 
					DOMSource sourceTime = new DOMSource(documentParameters); 
					StreamResult resultTime = new StreamResult(new File(parametersFile));
					transformerTime.transform(sourceTime, resultTime);
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				
				System.out.println("Starting experiments...");
				try {
					for(int alternatives = 25; alternatives>=5;alternatives = alternatives - 5){
						for(int services = 1000; services>=200; services = services - 200){
							for(int length = 5; length>=1; length = length - 1){
								String experimentalSetupFile =  "services_"+services+"_length_"+length+"_gateways_"+gateways+"_alternatives_"+alternatives+".dat";
								BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
								System.out.println("Experiment "+experimentalSetupFile);
								out.write("approach="+approach); out.newLine();
								out.write("summaryName=summary.xlsx"); out.newLine();
								out.write("parametersFile="+experimentalSetupFile); out.newLine();
								out.write("distributionType=0"); out.newLine();
								out.write("pathToData="+pathToData); out.newLine();
								out.write("hopsLimitGatewaysAdvertisement=3"); out.newLine();
								out.write("hopsLimitServiceAdvertisement=3"); out.newLine();
								out.write("hopsLimitServiceRequest=25"); out.newLine();
								out.write("distanceToUpdateRoutingTables=100"); out.newLine();
								out.write("checkGatewaysInterval=30m"); out.newLine();
								out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
								out.write("distanceToAddGateway=100"); out.newLine();
								out.write("numberOfServices="+services); out.newLine();
								out.write("distanceToRegisterService=100"); out.newLine();
								out.write("numberOfGateways="+gateways); out.newLine();
								out.write("pathToResults="+pathToResults); out.newLine();
								out.write("servicesAmount=10"); out.newLine();
								out.write("registrationInterval=5m"); out.newLine();
								out.write("serviceDensity=100"); out.newLine();
								out.write("numberOfRequests=100"); out.newLine();
								out.write("discoveryInterval=5m"); out.newLine();
								out.write("discoveryType=4"); out.newLine();
								out.write("similarityThreshold=10"); out.newLine();
								out.write("topK="+alternatives); out.newLine();
								out.write("length="+length); out.newLine();
								out.write("feedbackThreshold=0"); out.newLine();
								out.write("functionalThreshold=10"); out.newLine();
								out.write("gatewaysToAdvertise=5"); out.newLine();
								out.write("bothRoutingTables=1"); out.newLine();
								out.write("mobility="+mobility); out.newLine();
								out.close();
								String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
								for (int i = 0; i < lines.length;i++) { 
									if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
										lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
									}
								}
								out = new BufferedWriter(new FileWriter(actionsFile,false));
								for (int i = 0; i < lines.length; i++) {
									out.write(lines[i]); out.newLine(); 
								} 
								out.close();
								String s = null;
								//String e = null;
								Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , configFile});
								BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
								while ((s = stdInput.readLine()) != null) {
									System.out.println(s);
								}
								/*BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
								while ((e = stdError.readLine()) != null) {
								System.out.println(e);
								}*/
								removeFile(experimentalSetupFileBase+experimentalSetupFile);
							} 
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		System.out.println("Experiments Finished.");
	}
	
	private static void removeFile(String fileName) {
		File file = new File(fileName);
		file.delete();
	}
}
