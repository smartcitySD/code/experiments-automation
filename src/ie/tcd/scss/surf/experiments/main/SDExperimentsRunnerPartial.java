package ie.tcd.scss.surf.experiments.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SDExperimentsRunnerPartial {

	public static void main(String[] args) {

		/*String user = "ubuntu";
		String password = "ubuntu";*/
		String user = "cabrerac";
		String password = "Betho12983!";
		String host = "localhost";
		int port=22;
		JSch jsch = new JSch();
		String path = "../../Results/RawData/";

		/*String configFile = "../../../Simonstrator/simonstrator-simrunner/config/serviceDiscovery/serviceDiscovery_default.xml";
		String timeFile = "../../../Simonstrator/simonstrator-simrunner/config/serviceDiscovery/serviceDiscovery_eval_parameters.xml";
		String actionsFile =  "../../../Simonstrator/simonstrator-simrunner/config/serviceDiscovery/template.dat";*/
		String base = "../../../Simonstrator/simonstrator-simrunner/config/";
		String configFile = "serviceDiscovery_default.xml";
		String timeFile = "serviceDiscovery_eval_parameters.xml";
		String actionsFile =  "template.dat";
		String churnVariable = ""; 
		String approachVariable = ""; 
		String servicesVariable = ""; 
		String gatewaysVariable = ""; 
		String providersVariable = "";
		String citizensVariable = "";
		String hopsLimitVariable="";

		double total = 250;
		double current = 0;
		double progress;
		int time = 0;

		System.out.println("Starting experiments...");
		try {
			for (int approach = 3; approach <= 3; approach++) {
				configFile = "serviceDiscovery_default.xml";
				timeFile = "serviceDiscovery_eval_parameters.xml";
				actionsFile =  "template.dat";
				switch(approach){
				case 1:
					configFile = base + "serviceDiscoveryLocation/" + configFile;
					timeFile = base + "serviceDiscoveryLocation/" + timeFile;
					actionsFile = base + "serviceDiscoveryLocation/" + actionsFile;
					runWarming("location");
					break;
				case 2:
					configFile = base + "serviceDiscoveryDomain/" + configFile;
					timeFile = base + "serviceDiscoveryDomain/" + timeFile;
					actionsFile = base + "serviceDiscoveryDomain/" + actionsFile;
					runWarming("domain");
					break;
				case 3:
					configFile = base + "serviceDiscoverySmartSpace/" + configFile;
					timeFile = base + "serviceDiscoverySmartSpace/" + timeFile;
					actionsFile = base + "serviceDiscoverySmartSpace/" + actionsFile;
					runWarming("smartSpace");
					break;
				case 4:
					configFile = base + "serviceDiscoverySmartSpaceResponse/" + configFile;
					timeFile = base + "serviceDiscoverySmartSpaceResponse/" + timeFile;
					actionsFile = base + "serviceDiscoverySmartSpaceResponse/" + actionsFile;
					runWarming("smartSpaceResponse");
					break;
				}
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
				DocumentBuilder db = dbf.newDocumentBuilder(); 
				org.w3c.dom.Document documentConfig =db.parse(configFile); 
				org.w3c.dom.Document documentTime =	db.parse(timeFile);
				for (int gateways = 500; gateways <= 500; gateways = gateways + 100) {
					gatewaysVariable = "number-of-gateways-"+ gateways;
					int mobileGateways = gateways/2; 
					int staticGateways = gateways/2;
					for (int providers = 200; providers <= 200; providers = providers + 100) {
						providersVariable = "number-of-providers-"+ providers;
						int mobileProviders = providers/2; 
						int staticProviders = providers/2;
						for (int citizens = 100; citizens <= 100; citizens = citizens + 200) {
							citizensVariable = "number-of-citizens-"+ citizens;
							if (approach == 1) {
								approachVariable = "Location"; 
								String distanceToRegisterServiceVariable = ""; 
								for(int distanceToRegisterService = 100; distanceToRegisterService>=50; distanceToRegisterService= distanceToRegisterService - 10 ){
									distanceToRegisterServiceVariable="distance-register-service-"+distanceToRegisterService;
									for(int hopsLimit = 5; hopsLimit>=1;hopsLimit = hopsLimit - 1){
										hopsLimitVariable  ="hops-limit-"+hopsLimit;
										for (int churn = 1; churn >= 0; churn--) {
											if (churn == 0)churnVariable = "no-churn"; 
											else churnVariable = "churn";
											for (int i = 0; i <	  documentConfig.getElementsByTagName("Variable").getLength(); i++) {
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name") .getNodeValue().equals("CHURN")){ 
													if(churn==0)
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
													if(churn==1)
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
												}
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
												if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
												if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders); 
											}
											TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance(); 
											Transformer transformerConfig = transformerFactoryConfig.newTransformer(); 
											DOMSource sourceConfig = new DOMSource(documentConfig); 
											StreamResult resultConfig = new StreamResult(new File(configFile));
											transformerConfig.transform(sourceConfig, resultConfig);
											for (int services = 100000; services >= 20000; services = services - 20000) {
												servicesVariable = "number-of-services-"+services;
												String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRegisterServiceVariable + "/";;
												if(!existData(pathToRawData)){
													String[] lines = Files.readAllLines(new File(actionsFile).toPath()).toArray(new String[0]);
													for (int i = 0; i < lines.length;i++) { 
														if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"")) { 
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"", "StaticGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryStaticComponent:initializateGatewayLocation " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 50 + " " + distanceToRegisterService);
														}
														if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"")) { 
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"", "MobileGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryMobileComponent:initializateGatewayLocation " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 25 + " " + distanceToRegisterService);
														}
													}
													time = time + 120;
													for (int i = 0; i < lines.length;i++) {
														if (lines[i].contains("<NODE_INTIALIZATION>")) { 
															lines[i] = lines[i].replaceAll("<NODE_INTIALIZATION>", time + "m-" + (time + 60) + "m"); 
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length;i++) {
														if (lines[i].contains("<START_PUBLISHING>")) { 
															lines[i] = lines[i].replaceAll("<START_PUBLISHING>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													if (services == 20000){
														if(distanceToRegisterService==50)
															time = time + 720;
														if(distanceToRegisterService==60)
															time = time + 540;
														if(distanceToRegisterService==70)
															time = time + 420;
														if(distanceToRegisterService==80)
															time = time + 360;
														if(distanceToRegisterService==90)
															time = time + 330;
														if(distanceToRegisterService==100)
															time = time + 330;
													}
													if (services == 40000){
														if(distanceToRegisterService==50)
															time = time + 1380; 
														if(distanceToRegisterService==60)
															time = time + 1020;
														if(distanceToRegisterService==70)
															time = time + 780;
														if(distanceToRegisterService==80)
															time = time + 680;
														if(distanceToRegisterService==90)
															time = time + 630;
														if(distanceToRegisterService==100)
															time = time + 630;
													}
													if (services == 60000){
														if(distanceToRegisterService==50)
															time = time + 1980; 
														if(distanceToRegisterService==60)
															time = time + 1500;
														if(distanceToRegisterService==70)
															time = time + 1140;
														if(distanceToRegisterService==80)
															time = time + 1020;
														if(distanceToRegisterService==90)
															time = time + 960;
														if(distanceToRegisterService==100)
															time = time + 930;
													}
													if (services == 80000){
														if(distanceToRegisterService==50)
															time = time + 2640; 
														if(distanceToRegisterService==60)
															time = time + 1980;
														if(distanceToRegisterService==70)
															time = time + 1500;
														if(distanceToRegisterService==80)
															time = time + 1320;
														if(distanceToRegisterService==90)
															time = time + 1260;
														if(distanceToRegisterService==100)
															time = time + 1230;
													}
													if (services == 100000){
														if(distanceToRegisterService==50)
															time = time + 3120;
														if(distanceToRegisterService==60)
															time = time + 2460;
														if(distanceToRegisterService==70)
															time = time + 1860;
														if(distanceToRegisterService==80)
															time = time + 1650;
														if(distanceToRegisterService==90)
															time = time + 1500;
														if(distanceToRegisterService==100)
															time = time + 1500;
													}
													for (int i = 0; i < lines.length; i++) { 
														if(lines[i].contains("<STOP_PUBLISHING>")) { 
															lines[i] = lines[i].replaceAll("<STOP_PUBLISHING>", time + "m-" + (time + 60) + "m");
														} 
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {		
														if (lines[i].contains("<START_REQUESTS>")){ 
															lines[i] = lines[i].replaceAll("<START_REQUESTS>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 300;
													for (int i = 0; i < lines.length; i++) {
														if(lines[i].contains("<STOP_REQUESTS>")){
															lines[i] = lines[i].replaceAll("<STOP_REQUESTS>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_MESSAGES>")){ 
															lines[i] = lines[i].replaceAll("<PRINT_MESSAGES>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_NOT_RECEIVED>")){ 
															lines[i] = lines[i].replaceAll("<PRINT_NOT_RECEIVED>", time + "m-" + (time + 60) + "m");
														} 
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_RESULTS>")){
															lines[i] = lines[i].replaceAll("<PRINT_RESULTS>", time + "m-" + (time + 60) + "m");
														} 
														lines[i] = lines[i].replaceAll("<APPROACH>", approachVariable).replaceAll("<CHURN>", churnVariable) .replaceAll("<DIR_APPROACH>",approachVariable) .replaceAll("<SERVICES>", servicesVariable).replaceAll("<GATEWAYS>", gatewaysVariable).replaceAll("<PROVIDERS>", providersVariable).replaceAll("<CITIZENS>", citizensVariable).replaceAll("<HOPS>", hopsLimitVariable).replaceAll("<VARIABLE_PATH>",  distanceToRegisterServiceVariable);
													}
													time = time + 60;

													BufferedWriter out = new BufferedWriter(new FileWriter("../../../Simonstrator/simonstrator-simrunner/config/serviceDiscoveryLocation/serviceDiscoveryActions.dat",false));
													for (int i = 0; i < lines.length; i++) {
														out.write(lines[i]); out.newLine(); 
													} 
													out.close(); 
													for (int i = 0; i < documentTime.getElementsByTagName("Variable").getLength(); i++) { 
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + time + "m"); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN")) { 
															if (churn == 0) {
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
															} 
															if (churn == 1) {
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
															} 
														} 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN_START"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("60m");
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders);
													}
													TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
													Transformer transformerTime = transformerFactoryTime.newTransformer(); 
													DOMSource sourceTime = new DOMSource(documentTime); 
													StreamResult resultTime = new StreamResult(new File(timeFile));
													transformerTime.transform(sourceTime, resultTime);

													current++;
													/*System.out.println(current);
													System.out.println(churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+distanceToRegisterServiceVariable);*/
													Session session = jsch.getSession(user, host, port);
													session.setPassword(password);
													session.setConfig("StrictHostKeyChecking", "no");
													System.out.println("Establishing connection...");
													session.connect();
													System.out.println("Connection established.");
													ChannelShell channel=(ChannelShell) session.openChannel("shell");
													BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
													OutputStream outSSH = channel.getOutputStream();
													PrintStream commander = new PrintStream(outSSH, true);
													channel.connect();
													commander.println("cd ./Repositories/Simonstrator/simonstrator-simrunner/");
													commander.println("module load apps java/jdk/64/1.8.0_u101");
													commander.println("java -jar -Xms512M -Xmx20G simrunner-location.jar ./config/serviceDiscoveryLocation/serviceDiscovery_default.xml");
													commander.close();

													String msg=null;
													int k = 1;
													while((msg=in.readLine())!=null){
														progress = current/total;
														progress = progress * 100;
														if(k%5==0)
															System.out.println("***" + churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+ distanceToRegisterServiceVariable + "***");
														System.out.println("Experiment " + current + " out of " + total + " - " + progress + "% Completed: " + msg);
														if(msg.contains("SIMULATION FINISHED"))break;
														k++;
													}
													System.out.println("Experiment finished.");
													System.out.println("Disconnecting channel...");
													channel.disconnect();
													System.out.println("Channel disconnected.");
													System.out.println("Disconnecting session...");
													session.disconnect();
													System.out.println("Session disconnected.");
													time = 0; 
												}else{
													System.out.println("Already done");
													current++;
												}
											}
										}											
									}
								}
							}
							if (approach == 2) {
								approachVariable = "Domain"; 
								String numberOfDomainsVariable = "";
								for(int numberOfDomains = 5; numberOfDomains>=1; numberOfDomains= numberOfDomains -1){
									numberOfDomainsVariable="number-domains-"+numberOfDomains;
									for(int hopsLimit = 5; hopsLimit>=1;hopsLimit = hopsLimit - 1){
										hopsLimitVariable  ="hops-limit-"+hopsLimit;
										for (int churn = 1; churn >= 0; churn--) {
											if (churn == 0)churnVariable = "no-churn"; 
											else churnVariable = "churn";
											for (int i = 0; i <	  documentConfig.getElementsByTagName("Variable").getLength(); i++) {
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name") .getNodeValue().equals("CHURN")){ 
													if(churn==0)
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
													if(churn==1)
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
												}
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
												if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
												if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
												if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
													documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders); 
											}
											TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance(); 
											Transformer transformerConfig = transformerFactoryConfig.newTransformer(); 
											DOMSource sourceConfig = new DOMSource(documentConfig); 
											StreamResult resultConfig = new StreamResult(new File(configFile));
											transformerConfig.transform(sourceConfig, resultConfig);
											for (int services = 100000; services >= 20000; services = services - 20000) {
												servicesVariable = "number-of-services-"+services;
												String pathToRawData = path +  churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + numberOfDomainsVariable + "/";
												if(!existData(pathToRawData)){
													String[] lines = Files.readAllLines(new File(actionsFile).toPath()).toArray(new String[0]);
													for (int i = 0; i < lines.length;i++) { 
														if (lines[i].contains("_ECHO 0m \"<RECOGNISE_DOMAINS_STATIC>\"")) {
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<RECOGNISE_DOMAINS_STATIC>\"", "StaticGateways " + time +"m-" + (time + 60) + "m ServiceDiscoveryStaticComponent:getGatewayDomains " + numberOfDomains);
														}
														if (lines[i].contains("_ECHO 0m \"<RECOGNISE_DOMAINS_MOBILE>\"")) {
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<RECOGNISE_DOMAINS_MOBILE>\"", "MobileGateways " + time + "m-" + (time + 60) + "m ServiceDiscoveryMobileComponent:getGatewayDomains " + numberOfDomains);
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length;i++) {
														if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"")) { 
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"", "StaticGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryStaticComponent:initializateGatewayDomain " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit);
														}
														if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"")) { 
															lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"", "MobileGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryMobileComponent:initializateGatewayDomain " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit);
														}
													}
													time = time + 120;
													for (int i = 0; i < lines.length;i++) {
														if (lines[i].contains("<NODE_INTIALIZATION>")) { 
															lines[i] = lines[i].replaceAll("<NODE_INTIALIZATION>", time + "m-" + (time + 60) + "m"); 
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length;i++) {
														if (lines[i].contains("<START_PUBLISHING>")) { 
															lines[i] = lines[i].replaceAll("<START_PUBLISHING>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													if (services == 20000){
														if(numberOfDomains==1)
															time = time + 330; 
														if(numberOfDomains==2)
															time = time  + 330;
														if(numberOfDomains==3)
															time = time  + 330;
														if(numberOfDomains==4)
															time = time  + 330;
														if(numberOfDomains==5)
															time = time  + 330;
													}
													if (services == 40000){
														if(numberOfDomains==1)
															time = time + 660; 
														if(numberOfDomains==2)
															time = time  + 660;
														if(numberOfDomains==3)
															time = time  + 660;
														if(numberOfDomains==4)
															time = time  + 660;
														if(numberOfDomains==5)
															time = time  + 660;
													}
													if (services == 60000){
														if(numberOfDomains==1)
															time = time + 960; 
														if(numberOfDomains==2)
															time = time  + 960;
														if(numberOfDomains==3)
															time = time  + 960;
														if(numberOfDomains==4)
															time = time  + 960;
														if(numberOfDomains==5)
															time = time  + 960;
													}
													if (services == 80000){
														if(numberOfDomains==1)
															time = time + 1290; 
														if(numberOfDomains==2)
															time = time  + 1290;
														if(numberOfDomains==3)
															time = time  + 1290;
														if(numberOfDomains==4)
															time = time  + 1290;
														if(numberOfDomains==5)
															time = time  + 1290;
													}
													if (services == 100000){
														if(numberOfDomains==1)
															time = time + 1530; 
														if(numberOfDomains==2)
															time = time  + 1530;
														if(numberOfDomains==3)
															time = time  + 1530;
														if(numberOfDomains==4)
															time = time  + 1530;
														if(numberOfDomains==5)
															time = time  + 1530;
													}
													for (int i = 0; i < lines.length; i++) {
														if(lines[i].contains("<STOP_PUBLISHING>")) { 
															lines[i] = lines[i].replaceAll("<STOP_PUBLISHING>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<START_REQUESTS>")){ 
															lines[i] = lines[i].replaceAll("<START_REQUESTS>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 300;
													for (int i = 0; i < lines.length; i++) {
														if(lines[i].contains("<STOP_REQUESTS>")){
															lines[i] = lines[i].replaceAll("<STOP_REQUESTS>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_MESSAGES>")){ 
															lines[i] = lines[i].replaceAll("<PRINT_MESSAGES>", time + "m-" + (time + 60) + "m");
														}
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_NOT_RECEIVED>")){ 
															lines[i] = lines[i].replaceAll("<PRINT_NOT_RECEIVED>", time + "m-" + (time + 60) + "m");
														} 
													}
													time = time + 60;
													for (int i = 0; i < lines.length; i++) {
														if (lines[i].contains("<PRINT_RESULTS>")){
															lines[i] = lines[i].replaceAll("<PRINT_RESULTS>", time + "m-" + (time + 60) + "m");
														} 
														lines[i] = lines[i].replaceAll("<APPROACH>", approachVariable).replaceAll("<CHURN>", churnVariable) .replaceAll("<DIR_APPROACH>",approachVariable) .replaceAll("<SERVICES>", servicesVariable).replaceAll("<GATEWAYS>", gatewaysVariable).replaceAll("<PROVIDERS>", providersVariable).replaceAll("<CITIZENS>", citizensVariable).replaceAll("<HOPS>", hopsLimitVariable).replaceAll("<VARIABLE_PATH>", numberOfDomainsVariable); 
													}
													time = time + 60;

													BufferedWriter out = new BufferedWriter(new FileWriter("../../../Simonstrator/simonstrator-simrunner/config/serviceDiscoveryDomain/serviceDiscoveryActions.dat",false));
													for (int i = 0; i < lines.length; i++) {
														out.write(lines[i]); out.newLine(); 
													} 
													out.close(); 
													for (int i = 0; i < documentTime.getElementsByTagName("Variable").getLength(); i++) { 
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + time + "m"); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN")) { 
															if (churn == 0) {
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
															} 
															if (churn == 1) {
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
															} 
														} 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN_START"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("60m");
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
														if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
														if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
															documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders);
													}
													TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
													Transformer transformerTime = transformerFactoryTime.newTransformer(); 
													DOMSource sourceTime = new DOMSource(documentTime); 
													StreamResult resultTime = new StreamResult(new File(timeFile));
													transformerTime.transform(sourceTime, resultTime);

													current++;
													/*System.out.println(current);
													System.out.println(churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+numberOfDomainsVariable);*/

													Session session = jsch.getSession(user, host, port);
													session.setPassword(password);
													session.setConfig("StrictHostKeyChecking", "no");
													System.out.println("Establishing connection...");
													session.connect();
													System.out.println("Connection established.");
													ChannelShell channel=(ChannelShell) session.openChannel("shell");
													BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
													OutputStream outSSH = channel.getOutputStream();
													PrintStream commander = new PrintStream(outSSH, true);
													channel.connect();
													commander.println("cd ./Repositories/Simonstrator/simonstrator-simrunner/");
													commander.println("module load apps java/jdk/64/1.8.0_u101");
													commander.println("java -jar -Xms512M -Xmx20G simrunner-domain.jar ./config/serviceDiscoveryDomain/serviceDiscovery_default.xml");
													//commander.println("java -jar simrunner-domain.jar ./config/serviceDiscoveryDomain/serviceDiscovery_default.xml");
													commander.close();

													String msg=null;
													int k = 1;
													while((msg=in.readLine())!=null){
														progress = current/total;
														progress = progress * 100;
														if(k%5==0)
															System.out.println("***" + churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+numberOfDomainsVariable+"***");
														System.out.println("Experiment " + current + " out of " + total + " - " + progress + "% Completed: " + msg);
														if(msg.contains("SIMULATION FINISHED"))break;
														k++;
													}
													System.out.println("Experiment finished.");
													System.out.println("Disconnecting channel...");
													channel.disconnect();
													System.out.println("Channel disconnected.");
													System.out.println("Disconnecting session...");
													session.disconnect();
													System.out.println("Session disconnected.");
													time = 0; 
												}else{
													System.out.println("Already done");
													current++;
												}
											}
										}
									}
								}
							}
							if (approach == 3) {
								approachVariable = "SmartSpace";
								String distanceToRecogniseClosePlacesVariable = ""; 
								String topSimilarGatewaysVariable = "";
								for(int distanceToRecogniseClosePlaces = 100; distanceToRecogniseClosePlaces>=50; distanceToRecogniseClosePlaces= distanceToRecogniseClosePlaces - 10 ){
									distanceToRecogniseClosePlacesVariable="distance-close-places-"+distanceToRecogniseClosePlaces;
									for(int topSimilarGateways = 5; topSimilarGateways<=5; topSimilarGateways= topSimilarGateways + 1 ){
										topSimilarGatewaysVariable="top-similar-gateways-"+topSimilarGateways;
										for(int hopsLimit = 5; hopsLimit>=1;hopsLimit = hopsLimit - 1){
											hopsLimitVariable  ="hops-limit-"+hopsLimit;
											for (int churn = 1; churn >= 0; churn--) {
												if (churn == 0)churnVariable = "no-churn"; 
												else churnVariable = "churn";
												for (int i = 0; i <	  documentConfig.getElementsByTagName("Variable").getLength(); i++) {
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name") .getNodeValue().equals("CHURN")){ 
														if(churn==0)
															documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
														if(churn==1)
															documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
													}
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
													if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
													if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders); 
												}
												TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance(); 
												Transformer transformerConfig = transformerFactoryConfig.newTransformer(); 
												DOMSource sourceConfig = new DOMSource(documentConfig); 
												StreamResult resultConfig = new StreamResult(new File(configFile));
												transformerConfig.transform(sourceConfig, resultConfig);
												for (int services = 100000; services >= 20000; services = services - 20000) {
													servicesVariable = "number-of-services-"+services;
													String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable + "/";
													if(!existData(pathToRawData)){
														String[] lines = Files.readAllLines(new File(actionsFile).toPath()).toArray(new String[0]);
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("_ECHO 0m \"<RECOGNISE_PLACES_STATIC>\"")) {
																lines[i] = lines[i] .replaceAll("_ECHO 0m \"<RECOGNISE_PLACES_STATIC>\"", "StaticGateways " + time + "m-" + (time + 60) + "m ServiceDiscoveryStaticComponent:recogniseClosePlaces " + approach + " " + distanceToRecogniseClosePlaces);
															}
															if (lines[i].contains("_ECHO 0m \"<RECOGNISE_PLACES_MOBILE>\"")) {
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<RECOGNISE_PLACES_MOBILE>\"", "MobileGateways " + time + "m-" + (time + 60) + "m ServiceDiscoveryMobileComponent:recogniseClosePlaces " + approach + " " + distanceToRecogniseClosePlaces);
															}
														}
														time= time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"")) { 
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"", "StaticGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryStaticComponent:initializateGatewaySmartSpace " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 0 + " " + 0 + " " + 0 + " " + topSimilarGateways + " " +  topSimilarGateways);
															}
															if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"")) { 
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"", "MobileGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryMobileComponent:initializateGatewaySmartSpace " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 0 + " " + 0 + " " + 0 + " " + topSimilarGateways + " " +  topSimilarGateways);
															}
														}
														time= time + 120;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<NODE_INTIALIZATION>")) { 
																lines[i] = lines[i].replaceAll("<NODE_INTIALIZATION>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<START_PUBLISHING>")) { 
																lines[i] = lines[i].replaceAll("<START_PUBLISHING>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														if (services == 20000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 300;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 300;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 300; 
															}
														}
														if (services == 40000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 600;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 600;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 600; 
															} 
														}
														if (services == 60000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 840;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 840;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 840; 
															}
														}
														if (services == 80000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 1080;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 1080;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 1080; 
															}
														}
														if (services == 100000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 1350; 
															}
														}
														for (int i = 0; i < lines.length; i++) {
															if(lines[i].contains("<STOP_PUBLISHING>")) { 
																lines[i] = lines[i].replaceAll("<STOP_PUBLISHING>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<START_REQUESTS>")){ 
																lines[i] = lines[i].replaceAll("<START_REQUESTS>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 300;
														for (int i = 0; i < lines.length; i++) {
															if(lines[i].contains("<STOP_REQUESTS>")){
																lines[i] = lines[i].replaceAll("<STOP_REQUESTS>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_MESSAGES>")){ 
																lines[i] = lines[i].replaceAll("<PRINT_MESSAGES>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_NOT_RECEIVED>")){ 
																lines[i] = lines[i].replaceAll("<PRINT_NOT_RECEIVED>", time + "m-" + (time + 60) + "m");
															} 
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_RESULTS>")){
																lines[i] = lines[i].replaceAll("<PRINT_RESULTS>", time + "m-" + (time + 60) + "m");
															} 
															lines[i] = lines[i].replaceAll("<APPROACH>", approachVariable).replaceAll("<CHURN>", churnVariable) .replaceAll("<DIR_APPROACH>",approachVariable) .replaceAll("<SERVICES>", servicesVariable).replaceAll("<GATEWAYS>", gatewaysVariable).replaceAll("<PROVIDERS>", providersVariable).replaceAll("<CITIZENS>", citizensVariable).replaceAll("<HOPS>", hopsLimitVariable).replaceAll("<VARIABLE_PATH>", distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable); 
														}
														time = time + 60;
														BufferedWriter out = new BufferedWriter(new FileWriter("../../../Simonstrator/simonstrator-simrunner/config/serviceDiscoverySmartSpace/serviceDiscoveryActions.dat",false));
														for (int i = 0; i < lines.length; i++) {
															out.write(lines[i]); out.newLine(); 
														} 
														out.close(); 
														for (int i = 0; i < documentTime.getElementsByTagName("Variable").getLength(); i++) { 
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + time + "m"); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN")) { 
																if (churn == 0) {
																	documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
																} 
																if (churn == 1) {
																	documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
																} 
															} 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN_START"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("60m");
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders);
														}
														TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
														Transformer transformerTime = transformerFactoryTime.newTransformer(); 
														DOMSource sourceTime = new DOMSource(documentTime); 
														StreamResult resultTime = new StreamResult(new File(timeFile));
														transformerTime.transform(sourceTime, resultTime);

														current++;
														/*System.out.println(current);
													System.out.println(churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable);*/
														Session session = jsch.getSession(user, host, port);
														session.setPassword(password);
														session.setConfig("StrictHostKeyChecking", "no");
														System.out.println("Establishing connection...");
														session.connect();
														System.out.println("Connection established.");
														ChannelShell channel=(ChannelShell) session.openChannel("shell");
														BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
														OutputStream outSSH = channel.getOutputStream();
														PrintStream commander = new PrintStream(outSSH, true);
														channel.connect();
														commander.println("cd ./Repositories/Simonstrator/simonstrator-simrunner/");
														commander.println("module load apps java/jdk/64/1.8.0_u101");
														commander.println("java -jar -Xms512M -Xmx20G simrunner-smartSpace.jar ./config/serviceDiscoverySmartSpace/serviceDiscovery_default.xml");
														//commander.println("java -jar simrunner-smartSpace.jar ./config/serviceDiscoverySmartSpace/serviceDiscovery_default.xml");
														commander.close();

														String msg=null;
														int k = 1;
														while((msg=in.readLine())!=null){
															progress = current/total;
															progress = progress * 100;
															if(k%5==0)
																System.out.println("***" + churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable+"***");
															System.out.println("Experiment " + current + " out of " + total + " - " + progress + "% Completed: " + msg);
															if(msg.contains("SIMULATION FINISHED"))break;
															k++;
														}
														System.out.println("Experiment finished.");
														System.out.println("Disconnecting channel...");
														channel.disconnect();
														System.out.println("Channel disconnected.");
														System.out.println("Disconnecting session...");
														session.disconnect();
														System.out.println("Session disconnected.");
														time = 0; 
													}else{
														System.out.println("Already done");
														current++;
													}
												}
											}
										}
									}
								}
							}
							if (approach == 4) {
								approachVariable = "SmartSpaceResponse";
								String distanceToRecogniseClosePlacesVariable = ""; 
								String topSimilarGatewaysVariable = "";
								for(int distanceToRecogniseClosePlaces = 100; distanceToRecogniseClosePlaces>=50; distanceToRecogniseClosePlaces= distanceToRecogniseClosePlaces - 10 ){
									distanceToRecogniseClosePlacesVariable="distance-close-places-"+distanceToRecogniseClosePlaces;
									for(int topSimilarGateways = 5; topSimilarGateways<=5; topSimilarGateways= topSimilarGateways + 1 ){
										topSimilarGatewaysVariable="top-similar-gateways-"+topSimilarGateways;
										for(int hopsLimit = 5; hopsLimit>=1;hopsLimit = hopsLimit - 1){
											hopsLimitVariable  ="hops-limit-"+hopsLimit;
											for (int churn = 1; churn >= 0; churn--) {
												if (churn == 0)churnVariable = "no-churn"; 
												else churnVariable = "churn";
												for (int i = 0; i <	  documentConfig.getElementsByTagName("Variable").getLength(); i++) {
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name") .getNodeValue().equals("CHURN")){ 
														if(churn==0)
															documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
														if(churn==1)
															documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
													}
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
													if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
													if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
													if(documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
														documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders); 
												}
												TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance(); 
												Transformer transformerConfig = transformerFactoryConfig.newTransformer(); 
												DOMSource sourceConfig = new DOMSource(documentConfig); 
												StreamResult resultConfig = new StreamResult(new File(configFile));
												transformerConfig.transform(sourceConfig, resultConfig);
												for (int services = 100000; services >= 20000; services = services - 20000) {
													servicesVariable = "number-of-services-"+services;
													String pathToRawData = path + churnVariable + "/" + approachVariable + "/" + servicesVariable + "/" + gatewaysVariable + "/" +  providersVariable + "/" + citizensVariable + "/" + hopsLimitVariable + "/" + distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable + "/";
													if(!existData(pathToRawData)){
														String[] lines = Files.readAllLines(new File(actionsFile).toPath()).toArray(new String[0]);
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("_ECHO 0m \"<RECOGNISE_PLACES_STATIC>\"")) {
																lines[i] = lines[i] .replaceAll("_ECHO 0m \"<RECOGNISE_PLACES_STATIC>\"", "StaticGateways " + time + "m-" + (time + 60) + "m ServiceDiscoveryStaticComponent:recogniseClosePlaces " + approach + " " + distanceToRecogniseClosePlaces);
															}
															if (lines[i].contains("_ECHO 0m \"<RECOGNISE_PLACES_MOBILE>\"")) {
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<RECOGNISE_PLACES_MOBILE>\"", "MobileGateways " + time + "m-" + (time + 60) + "m ServiceDiscoveryMobileComponent:recogniseClosePlaces " + approach + " " + distanceToRecogniseClosePlaces);
															}
														}
														time= time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"")) { 
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_STATIC>\"", "StaticGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryStaticComponent:initializateGatewaySmartSpace " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 0 + " " + 0 + " " + 0 + " " + topSimilarGateways + " " +  topSimilarGateways);
															}
															if (lines[i].contains("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"")) { 
																lines[i] = lines[i].replaceAll("_ECHO 0m \"<NODE_INTIALIZATION_MOBILE>\"", "MobileGateways " + time +"m-" + (time + 120) + "m ServiceDiscoveryMobileComponent:initializateGatewaySmartSpace " + approach + " " + hopsLimit + " " + hopsLimit + " " + hopsLimit + " " + 0 + " " + 0 + " " + 0 + " " + topSimilarGateways + " " +  topSimilarGateways);
															}
														}
														time= time + 120;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<NODE_INTIALIZATION>")) { 
																lines[i] = lines[i].replaceAll("<NODE_INTIALIZATION>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<START_PUBLISHING>")) { 
																lines[i] = lines[i].replaceAll("<START_PUBLISHING>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														if (services == 20000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 300; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 300;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 300;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 300; 
															}
														}
														if (services == 40000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 600; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 600;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 600;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 600; 
															} 
														}
														if (services == 60000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 840; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 840;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 840;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 840; 
															}
														}
														if (services == 80000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 1080; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 1080;
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 1080;
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 1080; 
															}
														}
														if (services == 100000){
															if(distanceToRecogniseClosePlaces==50){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==60){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==70){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==80){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==90){
																time = time + 1350; 
															}
															if(distanceToRecogniseClosePlaces==100){
																time = time + 1350; 
															}
														}
														for (int i = 0; i < lines.length; i++) {
															if(lines[i].contains("<STOP_PUBLISHING>")) { 
																lines[i] = lines[i].replaceAll("<STOP_PUBLISHING>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<START_REQUESTS>")){ 
																lines[i] = lines[i].replaceAll("<START_REQUESTS>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 300;
														for (int i = 0; i < lines.length; i++) {
															if(lines[i].contains("<STOP_REQUESTS>")){
																lines[i] = lines[i].replaceAll("<STOP_REQUESTS>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_MESSAGES>")){ 
																lines[i] = lines[i].replaceAll("<PRINT_MESSAGES>", time + "m-" + (time + 60) + "m");
															}
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_NOT_RECEIVED>")){ 
																lines[i] = lines[i].replaceAll("<PRINT_NOT_RECEIVED>", time + "m-" + (time + 60) + "m");
															} 
														}
														time = time + 60;
														for (int i = 0; i < lines.length; i++) {
															if (lines[i].contains("<PRINT_RESULTS>")){
																lines[i] = lines[i].replaceAll("<PRINT_RESULTS>", time + "m-" + (time + 60) + "m");
															} 
															lines[i] = lines[i].replaceAll("<APPROACH>", approachVariable).replaceAll("<CHURN>", churnVariable) .replaceAll("<DIR_APPROACH>",approachVariable) .replaceAll("<SERVICES>", servicesVariable).replaceAll("<GATEWAYS>", gatewaysVariable).replaceAll("<PROVIDERS>", providersVariable).replaceAll("<CITIZENS>", citizensVariable).replaceAll("<HOPS>", hopsLimitVariable).replaceAll("<VARIABLE_PATH>", distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable); 
														}
														time = time + 60;
														BufferedWriter out = new BufferedWriter(new FileWriter("../../../Simonstrator/simonstrator-simrunner/config/serviceDiscoverySmartSpaceResponse/serviceDiscoveryActions.dat",false));
														for (int i = 0; i < lines.length; i++) {
															out.write(lines[i]); out.newLine(); 
														} 
														out.close(); 
														for (int i = 0; i < documentTime.getElementsByTagName("Variable").getLength(); i++) { 
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + time + "m"); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN")) { 
																if (churn == 0) {
																	documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("false"); 
																} 
																if (churn == 1) {
																	documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("true"); 
																} 
															} 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("CHURN_START"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("60m");
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) 
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+citizens);
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) 
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways); 
															if (documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticProviders); 
															if(documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
																documentTime.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileProviders);
														}
														TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
														Transformer transformerTime = transformerFactoryTime.newTransformer(); 
														DOMSource sourceTime = new DOMSource(documentTime); 
														StreamResult resultTime = new StreamResult(new File(timeFile));
														transformerTime.transform(sourceTime, resultTime);

														current++;
														/*System.out.println(current);
													System.out.println(churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable);*/
														Session session = jsch.getSession(user, host, port);
														session.setPassword(password);
														session.setConfig("StrictHostKeyChecking", "no");
														System.out.println("Establishing connection...");
														session.connect();
														System.out.println("Connection established.");
														ChannelShell channel=(ChannelShell) session.openChannel("shell");
														BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
														OutputStream outSSH = channel.getOutputStream();
														PrintStream commander = new PrintStream(outSSH, true);
														channel.connect();
														commander.println("cd ./Repositories/Simonstrator/simonstrator-simrunner/");
														commander.println("module load apps java/jdk/64/1.8.0_u101");
														commander.println("java -jar -Xms512M -Xmx20G simrunner-smartSpaceResponse.jar ./config/serviceDiscoverySmartSpaceResponse/serviceDiscovery_default.xml");
														//commander.println("java -jar simrunner-smartSpace.jar ./config/serviceDiscoverySmartSpace/serviceDiscovery_default.xml");
														commander.close();

														String msg=null;
														int k = 1;
														while((msg=in.readLine())!=null){
															progress = current/total;
															progress = progress * 100;
															if(k%5==0)
																System.out.println("***" + churnVariable+"/"+approachVariable+"/"+servicesVariable+"/"+gatewaysVariable+"/"+providersVariable+"/"+citizensVariable+"/"+hopsLimitVariable+"/"+distanceToRecogniseClosePlacesVariable + "/" + topSimilarGatewaysVariable+"***");
															System.out.println("Experiment " + current + " out of " + total + " - " + progress + "% Completed: " + msg);
															if(msg.contains("SIMULATION FINISHED"))break;
															k++;
														}
														System.out.println("Experiment finished.");
														System.out.println("Disconnecting channel...");
														channel.disconnect();
														System.out.println("Channel disconnected.");
														System.out.println("Disconnecting session...");
														session.disconnect();
														System.out.println("Session disconnected.");
														time = 0; 
													}else{
														System.out.println("Already done");
														current++;
													}
												}
											}
										}
									}
								}
							}
						} 
					}
				}
			}
		}
		catch (Exception e) { 
			e.printStackTrace(); 
		}
		System.out.println("Total Experiments: " + current);
		System.out.println("Experiments finished.");
		System.exit(0);

	}

	private static void runWarming(String approach) {
		System.out.println("running warming");
		/*String user = "ubuntu";
		String password = "ubuntu";*/
		String user = "cabrerac";
		String password = "Betho12983!";
		String host = "localhost";
		int port=22;
		JSch jsch = new JSch();
		String folder = "";
		if(approach.equals("location"))
			folder = "Location";
		if(approach.equals("domain"))
			folder = "Domain";
		if(approach.equals("smartSpace"))
			folder = "SmartSpace";
		if(approach.equals("smartSpaceResponse"))
			folder = "SmartSpaceResponse";
		try{
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			System.out.println("Establishing connection...");
			session.connect();
			System.out.println("Connection established.");
			ChannelShell channel=(ChannelShell) session.openChannel("shell");
			BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
			OutputStream outSSH = channel.getOutputStream();
			PrintStream commander = new PrintStream(outSSH, true);
			channel.connect();
			commander.println("cd ./Repositories/Simonstrator/simonstrator-simrunner/");
			commander.println("module load apps java/jdk/64/1.8.0_u101");
			//commander.println("java -jar -Xms512M -Xmx20G simrunner-"+approach+".jar ./config/serviceDiscovery"+folder+"/serviceDiscovery_warming.xml");
			commander.println("java -jar simrunner-"+approach+".jar ./config/serviceDiscovery"+folder+"/serviceDiscovery_warming.xml");
			commander.close();
			
			String msg=null;
			while((msg=in.readLine())!=null){
				System.out.println("Experiment warming " + approach + ": " + msg);
				if(msg.contains("SIMULATION FINISHED"))break;
			}
			System.out.println("Experiment finished.");
			System.out.println("Disconnecting channel...");
			channel.disconnect();
			System.out.println("Channel disconnected.");
			System.out.println("Disconnecting session...");
			session.disconnect();
			System.out.println("Session disconnected.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static boolean existData(String pathToRawData) {
		File folderResults = new File(pathToRawData);
		System.out.println("Analysing: " + pathToRawData + "messages");
		File[] folders = folderResults.listFiles();
		if(folders!=null){
			System.out.println("Data Available: " + pathToRawData + "messages");
			if(folders.length>=3){
				return true;
			}else{
				return false;
			}
		}else{
			System.out.println("No Data: " + pathToRawData);
			return false;
		}
	}
}
