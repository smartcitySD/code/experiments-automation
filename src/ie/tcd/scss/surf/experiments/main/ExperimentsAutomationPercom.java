package ie.tcd.scss.surf.experiments.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;

public class ExperimentsAutomationPercom {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		//String approach = args[0];
		//int mode = Integer.parseInt(args[1]);
		//int rounds = Integer.parseInt(args[2]);
		String approach = "urban";
		int mode = 1;
		int rounds = 1;		
		
		if(approach.equals("proximity"))
			approach = "location";
		
		if(approach.equals("biosocial"))
			approach = "domain";
		
		String base = "..";
		int requests = 100;
		int length = 1;
		int hops = 5;
		int create = 1;
		String time = "2500m";
		int adaptive = 1;
		int printErrors = 0;
		int intervalUtilityComputation = 1;
		int utilityTime = 500;
		int destinationSelection = 1;
		
		JSONObject parameters = new JSONObject();
		parameters.put("approach", approach);
		parameters.put("mode", mode);
		parameters.put("base", base);
		parameters.put("requests", requests);
		parameters.put("length", length);
		parameters.put("hops", hops);
		parameters.put("create", create);
		parameters.put("time", time);
		parameters.put("adaptive", adaptive);
		parameters.put("rounds", rounds);
		parameters.put("printErrors", printErrors);
		parameters.put("intervalUtilityComputation", intervalUtilityComputation);
		parameters.put("utilityTime", utilityTime);
		parameters.put("destinationSelection", destinationSelection);
		
		String configFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_default.xml";
		parameters.put("configFile", configFile);
		String parametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/serviceDiscovery_eval_parameters.xml";
		parameters.put("parametersFile", parametersFile);
		String pathToData = base + "/SmartCitySD/Data/";
		parameters.put("pathToData", pathToData);
		String pathToResults = base + "/SmartCitySD/Results/percom2020/";
		parameters.put("pathToResults", pathToResults);
		String simrunner =  base + "/Simonstrator/simonstrator-simrunner/simrunner-percom2020.jar";
		parameters.put("simrunner", simrunner);
		String templateFile =  "";
		String actionsFile =  "";
		String experimentalSetupFile = "";
		String initialParametersFile = "";
		
		switch(approach) {
			case "location":
				parameters.put("gatewaysToAdvertise", 5);
				parameters.put("distributionType", 1);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/actions.dat";
				parameters.put("actionsFile", actionsFile);
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/location-based/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("location",initialParametersFile);
				executeExperiments(parameters);
			break;
			case "domain":
				parameters.put("gatewaysToAdvertise", 3);
				parameters.put("distributionType", 2);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/actions.dat";
				parameters.put("actionsFile", actionsFile);
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/domain-based/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("domain",initialParametersFile);
				executeExperiments(parameters);
			break;
			case "urban":
				parameters.put("gatewaysToAdvertise", 5);
				parameters.put("distributionType", 3);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/actions.dat";
				parameters.put("actionsFile", actionsFile);
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("urban",initialParametersFile);
				executeExperiments(parameters);
			break;
			case "adaptive":
				parameters.put("gatewaysToAdvertise", 5);
				parameters.put("distributionType", 4);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/actions.dat";
				parameters.put("actionsFile", actionsFile);
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/urban-based-adaptive/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("urban",initialParametersFile);
				executeExperiments(parameters);
			break;
			case "interactions":
				parameters.put("gatewaysToAdvertise", 5);
				parameters.put("distributionType", 5);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/interactions-based/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/interactions-based/actions.dat";
				parameters.put("actionsFile", actionsFile);				
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/interactions-based/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/interactions-based/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("interactions",initialParametersFile);
				executeExperiments(parameters);
			break;
			case "apf":
				parameters.put("gatewaysToAdvertise", 3);
				parameters.put("distributionType", 6);
				
				templateFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/apf-based/template-"+adaptive+".dat";
				parameters.put("templateFile", templateFile);
				
				actionsFile =   base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/apf-based/actions.dat";
				parameters.put("actionsFile", actionsFile);		
				
				experimentalSetupFile =  base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/apf-based/";
				parameters.put("experimentalSetupFile", experimentalSetupFile);
				
				initialParametersFile = base + "/Simonstrator/simonstrator-simrunner/config/service-discovery/apf-based/initial-parameters/initial_parameters_"+mode+".dat";
				parameters.put("initialParametersFile", initialParametersFile);
				
				if(create==1)
					createInitialParametersFile("urban",initialParametersFile);
				executeExperiments(parameters);
			break;
		}
	}
	
	private static void executeExperiments(JSONObject parameters) {
		try {
			System.out.println("Starting experiments...");
			String simrunner = (String) parameters.get("simrunner");
			System.out.println("Simrunner: " + simrunner);
			System.out.println("Loading initial parameters...");
			String initialParametersFile = (String) parameters.get("initialParametersFile");
			Map<String,String> initialParameters = readInitialParametersFile(initialParametersFile);
			
			
			int initialGateways = Integer.parseInt(initialParameters.get("gateways"));
			int initialServices = Integer.parseInt(initialParameters.get("services"));
			
			Map<String,String> updatedParameters = new HashMap<String,String>(); 
			String actionsFileBase = (String) parameters.get("actionsFile");
			boolean firstTime = true;
			int rounds = (int) parameters.get("rounds");
			for(int round=0; round<rounds; round++){
				for (int gateways=500; gateways>=100; gateways = gateways - 200) {
					if(firstTime)
						gateways = initialGateways;
					updatedParameters.put("gateways", ""+gateways);
					for(int services=100000; services>=20000; services=services-20000) {
						if(firstTime)
							services = initialServices;
						firstTime = false;
						updatedParameters.put("services", ""+services);
						writeInitialParametersFile(initialParametersFile,updatedParameters);
						String approach = (String) parameters.get("approach");
						int mode = (int) parameters.get("mode");
						int destinationSelection = (int) parameters.get("destinationSelection");
						int hops = (int) parameters.get("hops");
						int adaptive = (int) parameters.get("adaptive");
						int requests = (int) parameters.get("requests");
						int intervalUtilityComputation = (int) parameters.get("intervalUtilityComputation");
						int utilityTime = (int) parameters.get("utilityTime");
						int printErrors = (int) parameters.get("printErrors");
						int distributionType = (int) parameters.get("distributionType");
						int gatewaysToAdvertise = (int) parameters.get("gatewaysToAdvertise");
						
						String experimentalSetupFileBase = (String) parameters.get("experimentalSetupFile");
						String experimentalSetupFile =  "mode_"+mode+"_services_"+services+"_gateways_"+gateways+".dat";
						String utilityFileName = experimentalSetupFile.replace(".dat", ".xlsx");
						String pathToResults = (String) parameters.get("pathToResults");
						String pathToData = (String) parameters.get("pathToData");
						String templateFile = (String) parameters.get("templateFile");
						String ap = approach;
						if(ap.equals("location"))
							ap = "proximity";
						if(ap.equals("domain"))
							ap = "biosocial";
						int rp = getRound(pathToResults+"utility-"+ap+"/"+utilityFileName);
						String actionsFile = actionsFileBase.replace(".dat", "_"+approach+"_"+mode+"_"+services+"_"+gateways+".dat");
						
						if(rp>=10)
							break;
						else 
							round = rp;
						System.out.println("Round " + (round+1) +"...");
						String configFile = (String) parameters.get("configFile");
						String parametersFile = (String) parameters.get("parametersFile");
						String  time = (String) parameters.get("time");
						switch(mode) {
						case 1:
							updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*1),(gateways*0),approach,mode,services,gateways);
							break;
						case 2:
							updateParametersFiles(configFile,parametersFile,time,actionsFile,(int)(gateways*0.5),(int)(gateways*0.5),approach,mode,services,gateways);
							break;
						case 3:
							updateParametersFiles(configFile,parametersFile,time,actionsFile,(gateways*0),(gateways*1),approach,mode,services,gateways);
							break;
						}
						
						BufferedWriter out = new BufferedWriter(new FileWriter(experimentalSetupFileBase+experimentalSetupFile,false));
						System.out.println("Experiment "+experimentalSetupFile);
						out.write("approach="+approach); out.newLine();
						out.write("mode="+mode); out.newLine();
						out.write("parametersFile="+experimentalSetupFile); out.newLine();
						out.write("distributionType="+distributionType); out.newLine();
						out.write("pathToData="+pathToData); out.newLine();
						out.write("hopsLimitGatewaysAdvertisement="+hops); out.newLine();
						out.write("hopsLimitServiceAdvertisement="+hops); out.newLine();
						out.write("hopsLimitServiceRequest="+hops); out.newLine();
						out.write("distanceToUpdateRoutingTables=100"); out.newLine();
						out.write("distanceToRecognisePlaces=100"); out.newLine();
						out.write("checkGatewaysInterval=30m"); out.newLine();
						out.write("checkGatewaysIntervalCitizen=1m"); out.newLine();
						out.write("distanceToAddGateway=100"); out.newLine();
						out.write("numberOfServices="+services); out.newLine();
						out.write("distanceToRegisterService=100"); out.newLine();
						out.write("numberOfDomains=5"); out.newLine();
						out.write("numberOfGateways="+gateways); out.newLine();
						out.write("pathToResults="+pathToResults); out.newLine();
						out.write("servicesAmount=10"); out.newLine();
						out.write("registrationInterval=5m"); out.newLine();
						out.write("numberOfRequests="+requests); out.newLine();
						out.write("serviceDensity=60"); out.newLine();
						out.write("discoveryInterval=5m"); out.newLine();
						out.write("discoveryType=4"); out.newLine();
						out.write("similarityThreshold=10"); out.newLine();
						out.write("topK=5"); out.newLine();
						out.write("length=1"); out.newLine();
						out.write("feedbackThreshold=10"); out.newLine();
						out.write("functionalThreshold=10"); out.newLine();
						out.write("similarityThresholdGateways=6"); out.newLine();
						out.write("similarityThresholdRegistration=6"); out.newLine();
						out.write("similarityThresholdRequest=6"); out.newLine();
						out.write("topSimilarGateways=10"); out.newLine();
						out.write("gatewaysToAdvertise="+gatewaysToAdvertise); out.newLine();
						out.write("bothRoutingTables=1"); out.newLine();
						out.write("superDomains=2"); out.newLine();
						out.write("adaptiveMode="+adaptive); out.newLine();
						out.write("adaptiveThreshold=1"); out.newLine();
						out.write("intervalUtilityComputation="+intervalUtilityComputation+"m"); out.newLine();
						out.write("requestsType=1"); out.newLine();
						out.write("gatewaySelection=3"); out.newLine();
						out.write("utilityFileName="+utilityFileName); out.newLine();
						out.write("round="+(round+1)); out.newLine();
						out.write("roundsLimit="+rounds); out.newLine();
						out.write("utilityTime="+utilityTime); out.newLine();
						out.write("destinationSelection="+destinationSelection); out.newLine();
						out.write("interest=7"); out.newLine();
						out.close();
						String[] lines = Files.readAllLines(new File(templateFile).toPath()).toArray(new String[0]);
						for (int i = 0; i < lines.length;i++) { 
							if (lines[i].contains("<PATH_TO_EXPERIMENTAL_SETUP_FILE>")) { 
								lines[i] = lines[i].replaceAll("<PATH_TO_EXPERIMENTAL_SETUP_FILE>", experimentalSetupFileBase+experimentalSetupFile);
							}
						}
						out = new BufferedWriter(new FileWriter(actionsFile,false));
						for (int i = 0; i < lines.length; i++) {
							out.write(lines[i]); out.newLine(); 
						} 
						out.close();
						String s = null;
						String e = null;
						String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+services+"_"+gateways+".xml");
						Process p=Runtime.getRuntime().exec(new String[]{"java", "-jar", simrunner , newConfigFile});
						BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
						while ((s = stdInput.readLine()) != null) {
							System.out.println(s);
						}
						if(printErrors==1){
							BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
							while ((e = stdError.readLine()) != null) {
								System.out.println(e);
							}
						}
						removeFile(experimentalSetupFileBase+experimentalSetupFile);
						removeFile(newConfigFile);
						String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+services+"_"+gateways+".xml");
						removeFile(newParametersFile);
					}
				}
			}
			System.out.println("Experiments Finished.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private static void updateParametersFiles(String configFile, String parametersFile, String finishedTime, String actionsFile, int staticGateways, 
			int mobileGateways, String approach, int mode, int services,int gateways) {
		System.out.println("Setting paremeters...");
		try{
			double minSpeed = 10/3.6;
			minSpeed = minSpeed * 10;
			minSpeed = Math.floor(minSpeed);
			minSpeed = minSpeed/10;
			double maxSpeed = 50/3.6;
			maxSpeed = maxSpeed * 10;
			maxSpeed = Math.floor(maxSpeed);
			maxSpeed = maxSpeed/10;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder(); 
			org.w3c.dom.Document documentParameters =db.parse(parametersFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("Variable").getLength(); i++) {
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("seed")){
					int seed = (int) (Math.random() * 1000);
					if(seed == 0)
						seed = 250;
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("" + seed);
				}
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("finishTime"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+finishedTime);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("actions"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(actionsFile);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-X"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2000");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("WORLD-Y"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("2000");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_CITIZENS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("0");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+staticGateways);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+mobileGateways);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("MOVEMENT"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("SOCIAL");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+minSpeed);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue(""+maxSpeed);
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MIN_CITIZEN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
				if(documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("SPEED_MAX_CITIZEN"))
					documentParameters.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("value").setNodeValue("1.5");
			}
			TransformerFactory transformerFactoryTime = TransformerFactory.newInstance(); 
			Transformer transformerTime = transformerFactoryTime.newTransformer(); 
			DOMSource sourceTime = new DOMSource(documentParameters); 
			String newParametersFile = parametersFile.replace(".xml", "_"+approach+"_"+mode+"_"+services+"_"+gateways+".xml");
			StreamResult resultTime = new StreamResult(new File(newParametersFile));
			transformerTime.transform(sourceTime, resultTime);
			
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder(); 
			documentParameters =db.parse(configFile);
			for (int i = 0; i <	  documentParameters.getElementsByTagName("xi:include").getLength(); i++) {
				if(documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("name").getNodeValue().equals("eval_parameters"))
					documentParameters.getElementsByTagName("xi:include").item(i).getAttributes().getNamedItem("href").setNodeValue(newParametersFile.replace("../Simonstrator/simonstrator-simrunner/config/service-discovery/", ""));
			}
			transformerFactoryTime = TransformerFactory.newInstance(); 
			transformerTime = transformerFactoryTime.newTransformer(); 
			sourceTime = new DOMSource(documentParameters); 
			String newConfigFile = configFile.replace(".xml", "_"+approach+"_"+mode+"_"+services+"_"+gateways+".xml");
			resultTime = new StreamResult(new File(newConfigFile));
			transformerTime.transform(sourceTime, resultTime);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static Map<String, String> readInitialParametersFile(String initialParametersFile) {
		HashMap<String,String> parameters = new HashMap<String,String>();
		try {
			String[] lines = Files.readAllLines(new File(initialParametersFile).toPath()).toArray(new String[0]);
			for (int i = 0; i < lines.length;i++) { 
				String key = lines[i].split("=")[0];
				String value = lines[i].split("=")[1];
				parameters.put(key, value);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return parameters;
	}
	
	private static void createInitialParametersFile(String approach, String initialParametersFile) {
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("services", "100000");
		parameters.put("gateways", "500");
		writeInitialParametersFile(initialParametersFile,parameters);
	}
	
	
	private static void writeInitialParametersFile(String initialParametersFile,
			Map<String, String> parameters) {
		try {
			File file = new File(initialParametersFile);
			file.getParentFile().mkdirs();
			file.createNewFile();
			FileWriter fw = new FileWriter(file,false);
			BufferedWriter out = new BufferedWriter(fw);
			parameters.forEach((key,value)->{
				try {
					out.write(key+"="+value); out.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			out.close();
			fw.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void removeFile(String fileName) {
		File file = new File(fileName);
		file.delete();
	}
	
	@SuppressWarnings("resource")
	private static int getRound(String fileName) {
		int round=0;
		File file = new File(fileName);
		if(file.exists()){
			try {
				Workbook utilityFile = new XSSFWorkbook(file);
				if(utilityFile.getNumberOfSheets()>4) {
					round=utilityFile.getNumberOfSheets()-4;
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return round;
	}

}
