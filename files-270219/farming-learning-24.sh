#!/bin/sh
#SBATCH -n 12
#SBATCH -t 72:00:00 
#SBATCH -p compute   # partition name
#SBATCH -J learn24 # sensible name for the job

# load the modules
source /etc/profile.d/modules.sh
module load staskfarm
module load default-gcc-openmpi-5.4.0-1.10.3
module load apps java/jdk/64/1.8.0_u101

# execute the commands via the slurm task farm wrapper
staskfarm commands_learning_24.txt
