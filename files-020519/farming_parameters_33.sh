#!/bin/sh
#SBATCH -n 12
#SBATCH -t 72:00:00 
#SBATCH -p compute   # partition name
#SBATCH -J urb3 # sensible name for the job

# load the modules
source /etc/profile.d/modules.sh
module load staskfarm
module load apps java/jdk/64/1.8.0_u101

# execute the commands via the slurm task farm wrapper
staskfarm commands_parameters_33.txt
